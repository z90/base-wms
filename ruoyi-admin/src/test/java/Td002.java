import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author zxg
 * @date 2023-10-08
 * @Desc
 */
public class Td002 {
    public static void main(String[] args) {
        String driver = "org.firebirdsql.jdbc.FBDriver";
        String url = "jdbc:firebirdsql://192.168.0.174:3050/C:/Program Files (x86)/xgxt2.0/PATROLDB.FDB?localEncoding=utf8";
        String userName = "sysdba";
        String pwd = "masterkey";
        Connection conn;
        Statement sql;
        ResultSet rs;
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(url,userName,pwd);
            sql = conn.createStatement();
            rs = sql.executeQuery("select * from USERS");
            while (rs.next()){
                System.out.println(rs.getInt("USERID"));
            }
            rs.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
