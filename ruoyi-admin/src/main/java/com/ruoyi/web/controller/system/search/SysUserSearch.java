package com.ruoyi.web.controller.system.search;

import com.ruoyi.common.core.search.HlxBaseSearch;
import lombok.Data;

/**
 * @author zxg
 * @date 2023/8/23
 * @Desc
 */
@Data
public class SysUserSearch extends HlxBaseSearch {

    private String userId;
}
