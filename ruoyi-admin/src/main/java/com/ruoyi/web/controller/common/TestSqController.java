package com.ruoyi.web.controller.common;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.service.SequenceService;
import com.ruoyi.common.enums.SequenceEnums;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zxg
 * @date 2023-11-09
 * @Desc
 */
@RestController
@RequestMapping("test2")
public class TestSqController extends BaseController {
    @Resource(name = "commonSequenceService")
    private SequenceService commonSequenceService;
    @GetMapping("index")
    public AjaxResult test(){
        System.out.println(commonSequenceService.generateSerialNumber(SequenceEnums.ORDER_CGTH.getCode()));;
        return AjaxResult.success("成功");
    }
}
