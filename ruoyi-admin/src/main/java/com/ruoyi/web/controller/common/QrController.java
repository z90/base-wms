package com.ruoyi.web.controller.common;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.framework.web.utils.JwtUtils;
import com.ruoyi.system.service.ISysConfigService;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zxg
 * @date 2023/2/27
 * @Desc
 */
@RestController
@RequestMapping("/qr")
@AllArgsConstructor
@Slf4j
public class QrController extends BaseController {

    private final ISysConfigService configService;

    /**
     * 获得二维码刷新时间
     * @return
     */
    @GetMapping(value = "/getQrTime")
    public AjaxResult getQrTime(){
        return success(configService.selectConfigByKey("hlx_qr_refresh"));
    }

    /**
     * 验证token是否过期
     * @param token
     * @return
     */
    @GetMapping(value = "/checkToken")
    public AjaxResult checkToken(String token){
        log.info("验证token信息：{}",token);
        Claims claims = JwtUtils.parseToken(token);
        Long l = claims.get("expire",Long.class);
        long nl = System.currentTimeMillis();
        if(nl>l){
            return AjaxResult.error("二维码已过期");
        }
        return AjaxResult.success(claims.get("venueCode"));
    }

    /**
     * 生产二维码
     * @param data
     * @param response
     */
    @GetMapping(value = "/createQr",produces = "image/png")
    public void createQr(String data, HttpServletResponse response){
        if(StrUtil.isEmpty(data)){
            return;
        }
        try {
            String apptUrl = configService.selectConfigByKey("hlx_appt_server");
            String time = configService.selectConfigByKey("hlx_qr_refresh");
            long lt = 30000L;
            if(StrUtil.isNotEmpty(time)){
                lt = Long.parseLong(time);
            }
            data = URLDecoder.decode(data.trim(),"utf8");
            Map<String,Object> map = new HashMap<>(2);
            map.put("venueCode",data);
            map.put("expire", System.currentTimeMillis() + lt);
            String token = JwtUtils.createToken(map);
            QrConfig qrConfig = new QrConfig(300,300);
            qrConfig.setCharset(StandardCharsets.UTF_8);
            qrConfig.setMargin(0);
            qrConfig.setErrorCorrection(ErrorCorrectionLevel.M);
            qrConfig.setRatio(1);
            response.setContentType("image/png");
            response.setCharacterEncoding("UTF-8");
            log.info("生成token信息：{}",token);
            QrCodeUtil.generate(apptUrl + "?token="+token,qrConfig,"PNG",response.getOutputStream());
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
