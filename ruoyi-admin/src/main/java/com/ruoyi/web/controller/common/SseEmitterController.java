package com.ruoyi.web.controller.common;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.po.SseEmitterPo;
import com.ruoyi.framework.web.utils.SseEmitterUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.*;

/**
 * 服务器端实时推送技术之 SseEmitter 的用法测试
 * <p>
 * 测试步骤:
 * 1.请求http://localhost:8888/sse/start?clientId=111接口,浏览器会阻塞,等待服务器返回结果;
 * 2.请求http://localhost:8888/sse/send?clientId=111接口,可以请求多次,并观察第1步的浏览器返回结果;
 * 3.请求http://localhost:8888/sse/end?clientId=111接口结束某个请求,第1步的浏览器将结束阻塞;
 * 其中clientId代表请求的唯一标志;
 *
 * @author zz
 */
@Slf4j
@RestController
@RequestMapping("/sse")
public class SseEmitterController {

    /**
     * 用于保存每个请求对应的 SseEmitter
      */


    /**
     * 返回SseEmitter对象
     *
     * @param clientId
     * @return
     */
    @RequestMapping(path = "/subscribe", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter subscribeSseEmitter() throws IOException {
        String uid = SecurityUtils.getLoginUser().getUser().getUserId().toString();
        log.info("clientId: {}",uid);
        SseEmitter sseEmitter =  SseEmitterUtils.createSseEmitter(uid);
        SseEmitterPo<List<Map<String,String>>> sseEmitterPo = new SseEmitterPo< List<Map<String,String>>>();
        List<Map<String,String>> lists = new ArrayList<>();
        Random random = new Random();
        for(int i=0;i<20;i++){
            Map<String,String> dataMap = new HashMap<>();
            dataMap.put("name","监测点#" + (i+1));
            dataMap.put("temp",(random.nextInt(2)/10f + 23f) +"℃");
            dataMap.put("humidness","45%");
            dataMap.put("airAp",(random.nextInt(1) +1000) + "pa");
            dataMap.put("airQuality","优");
            lists.add(dataMap);
        }
        sseEmitterPo.setData(lists);
        sseEmitter.send(JSON.toJSONString(sseEmitterPo));
        return sseEmitter;
    }

    /**
     * 向SseEmitter对象发送数据
     *
     * @param clientId
     * @return
     */
    @RequestMapping("/send")
    public String setSseEmitter(String msg) {
        SseEmitterPo< List<Map<String,String>>> sseEmitterPo = new SseEmitterPo< List<Map<String,String>>>();
        List<Map<String,String>> lists = new ArrayList<>();
        Random random = new Random();
        for(int i=0;i<12;i++){
            Map<String,String> dataMap = new HashMap<>();
            dataMap.put("name","监测点#" + (i+1));
            dataMap.put("temp",random.nextInt(100)+"℃");
            dataMap.put("humidness",random.nextInt(100)+"%");
            dataMap.put("airAp",random.nextInt(10) + "pa");
            dataMap.put("airQuality","优");
            lists.add(dataMap);
        }
        sseEmitterPo.setData(lists);
        SseEmitterUtils.sendMsgAllClient(sseEmitterPo);
        return "Succeed!";
    }

    /**
     * 将SseEmitter对象设置成完成
     *
     * @param clientId
     * @return
     */
    @RequestMapping("/end")
    public String completeSseEmitter(String clientId) {
        SseEmitterUtils.closeSseEmitter(clientId);
        return "Succeed!";
    }
}