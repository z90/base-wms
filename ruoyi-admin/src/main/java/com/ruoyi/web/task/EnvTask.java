package com.ruoyi.web.task;

import com.ruoyi.common.utils.po.SseEmitterPo;
import com.ruoyi.framework.web.utils.SseEmitterUtils;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author zxg
 * @date 2023-10-09
 * @Desc
 */
@Component("envTask")
public class EnvTask {

    public void sendEnvInfo()
    {
        SseEmitterPo<List<Map<String,String>>> sseEmitterPo = new SseEmitterPo< List<Map<String,String>>>();
        List<Map<String,String>> lists = new ArrayList<>();
        Random random = new Random();
        for(int i=0;i<20;i++){
            Map<String,String> dataMap = new HashMap<>();
            dataMap.put("name","监测点#" + (i+1));
            dataMap.put("temp",(random.nextInt(2)/10f + 23f) +"℃");
            dataMap.put("humidness","45%");
            dataMap.put("airAp",(random.nextInt(1) +1000) + "pa");
            dataMap.put("airQuality","优");
            lists.add(dataMap);
        }
        sseEmitterPo.setData(lists);
        SseEmitterUtils.sendMsgAllClient(sseEmitterPo);
    }
}
