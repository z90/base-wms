package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 仓库管理
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsWarehouseInfo extends WmsBaseEntity implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 仓库编码
     */
    @TableField("warehouse_code")
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @TableField("warehouse_name")
    private String warehouseName;

    /**
     * 仓库类型 字典表wms_warehouse_type
     */
    @TableField("warehouse_type")
    private String warehouseType;

    /**
     * 状态 0：禁用 10：可用 20：锁定
     */
    @TableField("warehouse_status")
    private String warehouseStatus;

    /**
     * 面积
     */
    private BigDecimal warehouseArea;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;

    /**
     * 地址
     */
    private String address;

}
