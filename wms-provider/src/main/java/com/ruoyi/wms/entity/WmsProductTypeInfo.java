package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商品分类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsProductTypeInfo extends WmsBaseEntity implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 类型名称
     */
    @TableField("type_name")
    private String typeName;

    /**
     * 类型编码
     */
    @TableField("type_code")
    private String typeCode;

    /**
     * 上级id
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * Y:启用 N:禁用
     */
    private String status;


}
