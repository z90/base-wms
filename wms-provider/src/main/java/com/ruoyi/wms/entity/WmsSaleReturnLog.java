package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 销售退货
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsSaleReturnLog extends WmsBaseEntity implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 客户编码
     */
    @TableField("customer_code")
    private String customerCode;

    /**
     * 客户名称
     */
    @TableField("customer_name")
    private String customerName;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 单位 字典 wms_product_unit
     */
    @TableField("product_unit")
    private String productUnit;

    /**
     * 产品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 业务编码
     */
    @TableField("business_code")
    private String businessCode;

    /**
     * 容器编码
     */
    @TableField("container_code")
    private String containerCode;

    /**
     * 数量
     */
    private BigDecimal num;


}
