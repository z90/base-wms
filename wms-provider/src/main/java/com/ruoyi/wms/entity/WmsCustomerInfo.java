package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 客户信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WmsCustomerInfo extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 客户编码
     */
    @TableField("customer_code")
    private String customerCode;

    /**
     * 客户名称
     */
    @TableField("customer_name")
    private String customerName;

    /**
     * 状态 Y: 启用 N:禁用
     */
    @TableField("customer_status")
    private String customerStatus;

    /**
     * 地址
     */
    private String address;

    /**
     * 邮编
     */
    private String postcode;


    /**
     * 传真
     */
    private String fax;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 类型
     */
    private String customerType;
    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;

}
