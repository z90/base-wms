package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 实时库存
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsLiveStockInfo extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 单位 字典 wms_product_unit
     */
    @TableField("product_unit")
    private String productUnit;

    /**
     * 产品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 容器编码
     */
    @TableField("container_code")
    private String containerCode;

    /**
     * 单价
     */
    private BigDecimal price;
    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 仓库编码
     */
    @TableField("warehouse_code")
    private String warehouseCode;

    /**
     * 库位编码
     */
    @TableField("location_code")
    private String locationCode;

    /**
     * 供应商编码
     */
    @TableField("supplier_code")
    private String supplierCode;

    /**
     * 是否锁定：N:否 Y:是
     */
    @TableField("lock_flag")
    private String lockFlag;

    /**
     * 乐观锁 
     */
    @Version
    private Integer version;

    /**
     * 类型 出库 入库 退货 报废等等
     */
    @TableField(exist = false)
    private String actionType;


}
