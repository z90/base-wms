package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 库位信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsWarehouseLocationInfo extends WmsBaseEntity implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 仓库id
     */
    @TableField("warehouse_id")
    private String warehouseId;

    /**
     * 库位编码
     */
    @TableField("location_code")
    private String locationCode;

    /**
     * 库位名称
     */
    @TableField("location_name")
    private String locationName;

    /**
     * 库位状态
     */
    private String locationStatus;


}
