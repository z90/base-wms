package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 移库记录
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsStockMoveLog extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 原库
     */
    @TableField("source_code")
    private String sourceCode;

    /**
     * 目标库
     */
    @TableField("target_code")
    private String targetCode;

    /**
     * 容器编码
     */
    @TableField("container_code")
    private String containerCode;

    /**
     * 数量
     */
    private BigDecimal num;


}
