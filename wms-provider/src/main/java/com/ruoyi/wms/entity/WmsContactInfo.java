package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 供应商联系人
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WmsContactInfo extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * id 供应商 or 客户id
     */
    @TableField("source_id")
    private String sourceId;

    /**
     * 联系人
     */
    @TableField("user_name")
    private String userName;

    /**
     * 联系电话
     */
    @TableField("user_tel")
    private String userTel;

    /**
     * 职务 字典：wms_contact_job
     */
    @TableField("duty_code")
    private String dutyCode;

    /**
     * 状态 Y: 启用 N:禁用
     */
    private String status;
    /**
     * 地址
     */
    private String address;



}
