package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 销售订单
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsSaleOrderInfo extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 订单号
     */
    @TableField("order_no")
    private String orderNo;
    /**
     * 订单名称
     */
    private String orderName;
    /**
     * 订单金额
     */
    private BigDecimal orderAmount;
    /**
     * 供应商编码
     */
    @TableField("customer_code")
    private String customerCode;
    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 物流单号
     */
    @TableField("flow_no")
    private String flowNo;

    /**
     * 状态 0：进行中 10：发货 20：完成 99：关闭
     */
    private Integer status;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;
    /**
     * 是否退货
     */
    private String isReturn;

    /**
     * 退款金额
     */
    private BigDecimal returnAmount;

    /**
     * 订单净额
     */
    private BigDecimal orderNetAmount;

}
