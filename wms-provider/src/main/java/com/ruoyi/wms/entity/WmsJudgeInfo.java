package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 盘点
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsJudgeInfo  extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 判断计划名称
     */
    @TableField("judge_name")
    private String judgeName;

    /**
     * 盘点类型 wms_judge_type A:全库 W:判断库位 P:判断产品 C:判断指定容器
     */
    @TableField("judge_type")
    private String judgeType;

    /**
     * 0: 新加 10：发布（进行中）20：完成 99：关闭
     */
    private Integer status;

    /**
     * Y:执行 N:未执行 用盘点结果调平库存
     */
    @TableField("execute_flag")
    private String executeFlag;




}
