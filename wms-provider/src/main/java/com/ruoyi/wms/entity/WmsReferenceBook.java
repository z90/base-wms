package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 备查簿
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsReferenceBook extends WmsBaseEntity implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 产品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 单位 字典 wms_product_unit
     */
    @TableField("product_unit")
    private String productUnit;

    /**
     * 更新数量
     */
    @TableField("update_num")
    private BigDecimal updateNum;

    /**
     * 更新前数量
     */
    @TableField("before_num")
    private BigDecimal beforeNum;

    /**
     * 更新后数量
     */
    @TableField("after_num")
    private BigDecimal afterNum;

    /**
     * 类型
     */
    @TableField("action_type")
    private String actionType;

    /**
     * 来源
     */
    @TableField("source_code")
    private String sourceCode;

    /**
     * 目标
     */
    @TableField("target_code")
    private String targetCode;


}
