package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 供应商
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsSupplierInfo extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 供应商编码
     */
    @TableField("supplier_code")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @TableField("supplier_name")
    private String supplierName;

    /**
     * 状态 Y: 启用 N:禁用
     */
    @TableField("supplier_status")
    private String supplierStatus;

    /**
     * 地址
     */
    private String address;
    /**
     * 传真
     */
    private String fax;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 类型
     */
    private String supplierType;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;

}
