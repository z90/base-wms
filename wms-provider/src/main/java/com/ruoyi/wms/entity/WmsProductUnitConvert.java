package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 单位转换表
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsProductUnitConvert extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    @TableField(exist = false)
    private String productName;
    /**
     * 原单位
     */
    @TableField("from_unit")
    private String fromUnit;

    /**
     * 转换单位
     */
    @TableField("to_unit")
    private String toUnit;

     /**
     * 转换比
     */
    @TableField("convert_num")
    private BigDecimal convertNum;


}
