package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单详情
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsOrderDetailInfo extends WmsBaseEntity implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 订单号
     */
    @TableField("order_no")
    private String orderNo;
    /**
     * 单价
     */
    private BigDecimal orderPrice;
    /**
     * 实际价格 默认和 制单价一样
     */
    private BigDecimal actualPrice;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 产品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 单位 字典 wms_product_unit
     */
    @TableField("product_unit")
    private String productUnit;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 是否退货Y:是N:否
     */
    private String isReturn;

    /**
     * 完成数量
     */
    private BigDecimal completeNum;

    /**
     * 退货数量
     */
    private BigDecimal returnNum;

    /**
     * 状态 0：进行中 10：完成 99：关闭
     */
    private Integer status;


    /**
     * 源仓库编码
     */
    private String sourceWarehouseCode;

    /**
     * 源仓库名称
     */
    private String sourceWarehouseName;
    /**
     * 源库位编号
     */
    private String sourceLocationCode;
    /**
     * 源库位名称
     */
    private String sourceLocationName;

    /**
     * 目标仓库编码
     */
    private String targetWarehouseCode;

    /**
     * 目标仓库名称
     */
    private String targetWarehouseName;
    /**
     * 目标库位编号
     */
    private String targetLocationCode;
    /**
     * 目标库位名称
     */
    private String targetLocationName;

}
