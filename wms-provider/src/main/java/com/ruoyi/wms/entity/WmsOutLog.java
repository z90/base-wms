package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 出库记录
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsOutLog extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 单位 字典 wms_product_unit
     */
    @TableField("product_unit")
    private String productUnit;

    /**
     * 产品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 业务编码
     */
    @TableField("business_code")
    private String businessCode;

    /**
     * 容器编码
     */
    @TableField("container_code")
    private String containerCode;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 类型 wms_out_action
     */
    @TableField("action_type")
    private String actionType;

    /**
     * 来源 库位
     */
    @TableField("source_code")
    private String sourceCode;

    /**
     * 来源名称
     */
    @TableField("source_name")
    private String sourceName;

    /**
     * 目标库位 库位 or 客户
     */
    @TableField("target_code")
    private String targetCode;

    /**
     * 目标名称
     */
    @TableField("target_name")
    private String targetName;


}
