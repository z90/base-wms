package com.ruoyi.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsProductInfo extends WmsBaseEntity implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 产品编码
     */
    @TableField("product_code")
    private String productCode;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 产品类型
     */
    @TableField("product_type")
    private String productType;

    /**
     * 单位 字典 wms_product_unit
     */
    @TableField("product_unit")
    private String productUnit;

    /**
     * 来源 wms_product_source
     */
    private String source;

    /**
     * 最小库存
     */
    @TableField("min_stock")
    private BigDecimal minStock;

    /**
     * 最大库存
     */
    @TableField("max_stock")
    private BigDecimal maxStock;

    /**
     * 状态 Y: 启用 N:禁用
     */
    @TableField("lock_flag")
    private String lockFlag;

    /**
     * 保质期
     */
    private Integer advent;

    /**
     * 状态 Y: 启用 N:禁用
     */
    private String status;

    /**
     * 状态 Y: 启用 N:禁用
     */
    @TableField("is_fifo")
    private String isFifo;

    /**
     * 规格
     */
    private String specUnit;

}
