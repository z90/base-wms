package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 采购订单
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsPurchaseOrderInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单名称
     */
    private String orderName;
    /**
     * 供应商编码
     */
    private String supplierCode;
    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;
    /**
     * 是否退货
     */
    private String isReturn;

    /**
     * 退款金额
     */
    private BigDecimal returnAmount;
    /**
     * 状态 0：进行中 10：完成 99：关闭
     */
    private Integer status;

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;


    /**
     * 订单净额
     */
    private BigDecimal orderNetAmount;
}
