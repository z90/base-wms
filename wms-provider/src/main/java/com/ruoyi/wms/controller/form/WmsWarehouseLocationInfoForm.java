package com.ruoyi.wms.controller.form;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 库位信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsWarehouseLocationInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
              /**
     * 仓库id
     */
   // @NotEmpty(message = "仓库id不能为空")
    private String warehouseId;
        /**
     * 库位编码
     */
   // @NotEmpty(message = "库位编码不能为空")
    private String locationCode;
        /**
     * 库位名称
     */
   // @NotEmpty(message = "库位名称不能为空")
    private String locationName;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

    /**
     * 库位状态
     */
    private String locationStatus;

  }
