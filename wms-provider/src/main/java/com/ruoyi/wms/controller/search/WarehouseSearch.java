package com.ruoyi.wms.controller.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-06
 * @Desc
 */
@Data
public class WarehouseSearch {

    /**
     * 仓库编码
     */
    private String warehouseCode;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * 状态
     */
    private String warehouseStatus;
}
