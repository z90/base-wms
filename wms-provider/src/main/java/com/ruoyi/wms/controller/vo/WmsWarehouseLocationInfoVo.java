package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 库位信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsWarehouseLocationInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 仓库id
     */
    private String warehouseId;
    /**
     * 库位编码
     */
    private String locationCode;
    /**
     * 库位名称
     */
    private String locationName;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;


    /**
     * 库位状态
     */
    private String locationStatus;
}
