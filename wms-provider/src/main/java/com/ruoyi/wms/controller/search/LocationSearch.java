package com.ruoyi.wms.controller.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-06
 * @Desc
 */
@Data
public class LocationSearch {

    /**
     * 仓库编码
     */
    private String locationCode;

    /**
     * 仓库名称
     */
    private String locationName;

     /**
     * 所属仓库
     */
    private String warehouseId;
}
