package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 移库记录
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsStockMoveLogVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private Long id;
    /**
     * 产品编码
     */
    private String productCode;
    /**
     * 原库
     */
    private String sourceCode;
    /**
     * 目标库
     */
    private String targetCode;
    /**
     * 容器编码
     */
    private String containerCode;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
}
