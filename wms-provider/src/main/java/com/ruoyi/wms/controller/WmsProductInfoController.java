package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.search.HlxBaseSearch;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wms.controller.excel.WmsProductInfoExcel;
import com.ruoyi.wms.controller.form.WmsProductInfoForm;
import com.ruoyi.wms.controller.search.ProductSearch;
import com.ruoyi.wms.entity.WmsProductTypeInfo;
import com.ruoyi.wms.mapstruct.WmsDictMapStructMapper;
import com.ruoyi.wms.mapstruct.WmsProductInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsProductInfoService;



import com.ruoyi.wms.entity.WmsProductInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 商品表 前端控制器实体类（WmsProductInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsProductInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsProductInfoController extends BaseController {
    private final IWmsProductInfoService iWmsProductInfoService;
    private final WmsProductInfoMapStructMapper wmsProductInfoMapStructMapper;
    private final WmsDictMapStructMapper wmsDictMapStructMapper;

    /**
     * 新增商品表
     * @param form
     * @return
     */
    @PostMapping("saveWmsProductInfo")
    public AjaxResult saveWmsProductInfo(@Valid @RequestBody WmsProductInfoForm form){
        WmsProductInfo wmsProductInfo = wmsProductInfoMapStructMapper.formToWmsProductInfo(form);
        WmsProductInfo info = iWmsProductInfoService.getByCode(form.getProductCode());
        if(info!=null){
            return AjaxResult.error("商品编号已存在");
        }
        iWmsProductInfoService.save(wmsProductInfo);
        return AjaxResult.success();
    }
    /**
     * 商品表删除
     * @param id
     * @return
     */
    @DeleteMapping("/delWmsProductInfo/{id}")
    public AjaxResult delWmsProductInfo(@PathVariable String id){
        iWmsProductInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 商品表修改
    * @return
    */
    @PutMapping("updWmsProductInfo")
    public AjaxResult updWmsProductInfo(@Valid @RequestBody WmsProductInfoForm form){
        WmsProductInfo wmsProductInfo = wmsProductInfoMapStructMapper.formToWmsProductInfo(form);
        WmsProductInfo info = iWmsProductInfoService.getByCode(form.getProductCode());
        if(info!=null && !info.getId().equals(form.getId())){
            return AjaxResult.error("商品编号已存在");
        }
        iWmsProductInfoService.updateById(wmsProductInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得商品表列表
     * @param id
     * @return
     */
    @RequestMapping("findWmsProductInfoById/{id}")
    public AjaxResult findWmsProductInfoById(@PathVariable String id){
        return AjaxResult.success(iWmsProductInfoService.getById(id));
    }

    /**
     * 获得商品表详情通过ID
     * @return AjaxResult(WmsProductInfo)
     */
    @RequestMapping("findWmsProductInfoList")
    public TableDataInfo findWmsProductInfoList(ProductSearch search){
        startPage();
        List<WmsProductInfo> list = iWmsProductInfoService.list(
                Wrappers.<WmsProductInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getProductName()),WmsProductInfo::getProductName,search.getProductName())
                        .like(StrUtil.isNotEmpty(search.getProductCode()),WmsProductInfo::getProductCode,search.getProductCode())
                        .eq(StrUtil.isNotEmpty(search.getProductType()),WmsProductInfo::getProductType,search.getProductType())
                        .orderByDesc(WmsProductInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 通过商品类型获得 商品列表
     * @param type 类型编码
     * @return 商品列表
     */
    @GetMapping("/getDictListByType/{type}")
    public AjaxResult getDictListByType(@PathVariable String type){
        List<WmsProductInfo> list = iWmsProductInfoService.list(
                Wrappers.<WmsProductInfo>lambdaQuery().eq(WmsProductInfo::getProductType,type).orderByAsc(WmsProductInfo::getOrderBy).orderByDesc(WmsProductInfo::getCreateDate)
        );
        return AjaxResult.success(wmsDictMapStructMapper.productsToDicts(list));
    }

    /**
     * 获得全部产品
     * @return
     */
    @GetMapping("/getDictList")
    public AjaxResult getDictList(){
        List<WmsProductInfo> list = iWmsProductInfoService.list(
                Wrappers.<WmsProductInfo>lambdaQuery().orderByAsc(WmsProductInfo::getOrderBy).orderByDesc(WmsProductInfo::getCreateDate)
        );
        return AjaxResult.success(wmsDictMapStructMapper.productsToDicts(list));
    }

    /**
     * 下载产品导入模板
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<WmsProductInfoExcel> util = new ExcelUtil<WmsProductInfoExcel>(WmsProductInfoExcel.class);
        util.importTemplateExcel(response, "产品数据");
    }

    /**
     * 产品导出
     * @param response
     * @param search
     */
    @Log(title = "产品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductSearch search)
    {
        List<WmsProductInfo> list = iWmsProductInfoService.list(
                Wrappers.<WmsProductInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getProductName()),WmsProductInfo::getProductName,search.getProductName())
                        .like(StrUtil.isNotEmpty(search.getProductCode()),WmsProductInfo::getProductCode,search.getProductCode())
                        .eq(StrUtil.isNotEmpty(search.getProductType()),WmsProductInfo::getProductType,search.getProductType())
                        .orderByDesc(WmsProductInfo::getCreateDate)
        );
        ExcelUtil<WmsProductInfoExcel> util = new ExcelUtil<WmsProductInfoExcel>(WmsProductInfoExcel.class);
        util.exportExcel(response, wmsProductInfoMapStructMapper.infosToExcels(list), "产品数据");
    }

    /**
     * 产品导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "产品管理", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<WmsProductInfoExcel> util = new ExcelUtil<WmsProductInfoExcel>(WmsProductInfoExcel.class);
        List<WmsProductInfoExcel> excels = util.importExcel(file.getInputStream());
        String message = iWmsProductInfoService.importProduct(wmsProductInfoMapStructMapper.excelsToInfos(excels),updateSupport);
        return success(message);
    }

    /**
     * 获得产品列表
     * @param search 查询条件
     * @return 产品列表
     */
    @GetMapping("/getProductList")
    public AjaxResult getProductList(HlxBaseSearch search){
        List<WmsProductInfo> list = iWmsProductInfoService.list(
                Wrappers.<WmsProductInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getKeyword()),WmsProductInfo::getProductName,search.getKeyword())
                        .orderByDesc(WmsProductInfo::getCreateDate).last("limit 15")
        );
        return success(wmsProductInfoMapStructMapper.wmsProductInfoToVo(list));
    }
}

