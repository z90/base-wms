package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wms.controller.form.WmsWarehouseLocationInfoForm;
import com.ruoyi.wms.controller.search.LocationSearch;
import com.ruoyi.wms.entity.WmsWarehouseInfo;
import com.ruoyi.wms.mapstruct.WmsWarehouseLocationInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsWarehouseLocationInfoService;
import com.ruoyi.wms.entity.WmsWarehouseLocationInfo;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 库位信息 前端控制器实体类（WmsWarehouseLocationInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsWarehouseLocationInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsWarehouseLocationInfoController extends BaseController {
    private final IWmsWarehouseLocationInfoService iWmsWarehouseLocationInfoService;
    private final WmsWarehouseLocationInfoMapStructMapper wmsWarehouseLocationInfoMapStructMapper;

    /**
     * 新增库位信息
     * @param form
     * @return
     */
    @PostMapping("saveWmsWarehouseLocationInfo")
    public AjaxResult saveWmsWarehouseLocationInfo(@Valid @RequestBody WmsWarehouseLocationInfoForm form){
        WmsWarehouseLocationInfo info = iWmsWarehouseLocationInfoService.getByCode(form.getLocationCode());
        WmsWarehouseLocationInfo wmsWarehouseLocationInfo = wmsWarehouseLocationInfoMapStructMapper.formToWmsWarehouseLocationInfo(form);
        iWmsWarehouseLocationInfoService.save(wmsWarehouseLocationInfo);
        return AjaxResult.success();
    }
    /**
     * 库位信息删除
     * @param id
     * @return
     */
    @DeleteMapping("/delWmsWarehouseLocationInfo/{id}")
    public AjaxResult delWmsWarehouseLocationInfo(@PathVariable String id){
        iWmsWarehouseLocationInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 库位信息修改
    * @return
    */
    @PutMapping("updWmsWarehouseLocationInfo")
    public AjaxResult updWmsWarehouseLocationInfo(@Valid @RequestBody WmsWarehouseLocationInfoForm form){
        WmsWarehouseLocationInfo wmsWarehouseLocationInfo = wmsWarehouseLocationInfoMapStructMapper.formToWmsWarehouseLocationInfo(form);
        iWmsWarehouseLocationInfoService.updateById(wmsWarehouseLocationInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得库位信息列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsWarehouseLocationInfoList")
    public TableDataInfo findWmsWarehouseLocationInfoList(LocationSearch search){
        startPage();
        List<WmsWarehouseLocationInfo> list = iWmsWarehouseLocationInfoService.list(
                Wrappers.<WmsWarehouseLocationInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getLocationCode()),WmsWarehouseLocationInfo::getLocationCode,search.getLocationCode())
                        .like(StrUtil.isNotEmpty(search.getLocationName()),WmsWarehouseLocationInfo::getLocationName,search.getLocationName())
                        .eq(StrUtil.isNotEmpty(search.getWarehouseId()),WmsWarehouseLocationInfo::getWarehouseId,search.getWarehouseId())
                        .orderByDesc(WmsWarehouseLocationInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得库位信息详情通过ID
     * @return AjaxResult(WmsWarehouseLocationInfo)
     */
    @RequestMapping("findWmsWarehouseLocationInfoById/{id}")
    public AjaxResult findWmsWarehouseLocationInfoById(@PathVariable String id){
        WmsWarehouseLocationInfo wmsWarehouseLocationInfo = iWmsWarehouseLocationInfoService.getById(id);
        return AjaxResult.success(wmsWarehouseLocationInfo);
    }
}

