package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsProductInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 产品编码
     */
    private String productCode;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品类型
     */
    private String productType;
    /**
     * 单位 字典 wms_product_unit
     */
    private String productUnit;
    /**
     * 来源 wms_product_source
     */
    private String source;
    /**
     * 最小库存
     */
    private BigDecimal minStock;
    /**
     * 最大库存
     */
    private BigDecimal maxStock;
    /**
     * 状态 Y: 启用 N:禁用
     */
    private String lockFlag;
    /**
     * 保质期
     */
    private Integer advent;
    /**
     * 状态 Y: 启用 N:禁用
     */
    private String status;
    /**
     * 状态 Y: 启用 N:禁用
     */
    private String isFifo;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
    /**
     * 规格
     */
    private String specUnit;
}
