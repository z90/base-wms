package com.ruoyi.wms.controller.vo;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-08-18
 * @Desc
 */
@Data
public class WmsDictRawVo {
    private String listClass = "";
}
