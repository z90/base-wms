package com.ruoyi.wms.controller.form;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 供应商
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsSupplierInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
              /**
     * 供应商编码
     */
   // @NotEmpty(message = "供应商编码不能为空")
    private String supplierCode;
        /**
     * 供应商名称
     */
   // @NotEmpty(message = "供应商名称不能为空")
    private String supplierName;
        /**
     * 状态 Y: 启用 N:禁用
     */
   // @NotEmpty(message = "状态 Y: 启用 N:禁用不能为空")
    private String supplierStatus;
        /**
     * 地址
     */
   // @NotEmpty(message = "地址不能为空")
    private String address;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;
    /**
     * 传真
     */
    private String fax;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 类型
     */
    private String supplierType;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;
  }
