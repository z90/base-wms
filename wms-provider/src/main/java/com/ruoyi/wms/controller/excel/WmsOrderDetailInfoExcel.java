package com.ruoyi.wms.controller.excel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单详情
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsOrderDetailInfoExcel implements Serializable {

    /**
     * 订单号
     */
    @Excel(name = "订单号")
    private String orderNo;
    /**
     * 单价
     */
    @Excel(name = "制单价")
    private BigDecimal orderPrice;
    /**
     * 实际价格 默认和 制单价一样
     */
    @Excel(name = "入库价")
    private BigDecimal actualPrice;

    /**
     * 产品编码
     */
    @TableField("product_code")
    @Excel(name = "产品编码")
    private String productCode;

    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 单位 字典 wms_product_unit
     */
    @Excel(name = "单位")
    private String productUnit;
    /**
     * 数量
     */
    @Excel(name = "数量")
    private BigDecimal num;

    /**
     * 完成数量
     */
    @Excel(name = "完成数量")
    private BigDecimal completeNum;

    /**
     * 退货数量
     */
    @Excel(name = "退货数量")
    private BigDecimal returnNum;
}
