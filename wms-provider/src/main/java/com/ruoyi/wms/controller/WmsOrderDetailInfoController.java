package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wms.controller.excel.WmsCustomerInfoExcel;
import com.ruoyi.wms.controller.excel.WmsOrderDetailInfoExcel;
import com.ruoyi.wms.controller.form.WmsOrderDetailInfoForm;
import com.ruoyi.wms.controller.search.CustomerSearch;
import com.ruoyi.wms.controller.search.OrderSearch;
import com.ruoyi.wms.entity.WmsCustomerInfo;
import com.ruoyi.wms.mapstruct.WmsOrderDetailInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsOrderDetailInfoService;
import com.ruoyi.wms.entity.WmsOrderDetailInfo;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 订单详情 前端控制器实体类（WmsOrderDetailInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsOrderDetailInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsOrderDetailInfoController extends BaseController {
    private final IWmsOrderDetailInfoService iWmsOrderDetailInfoService;
    private final WmsOrderDetailInfoMapStructMapper wmsOrderDetailInfoMapStructMapper;

    /**
     * 新增订单详情
     * @param form
     * @return
     */
    @PostMapping("saveWmsOrderDetailInfo")
    public AjaxResult saveWmsOrderDetailInfo(@Valid @RequestBody WmsOrderDetailInfoForm form){
        WmsOrderDetailInfo wmsOrderDetailInfo = wmsOrderDetailInfoMapStructMapper.formToWmsOrderDetailInfo(form);
        iWmsOrderDetailInfoService.save(wmsOrderDetailInfo);
        return AjaxResult.success();
    }
    /**
     * 订单详情删除
     * @param id 订单id
     * @return
     */
    @DeleteMapping("/delWmsOrderDetailInfo/{id}")
    public AjaxResult delWmsOrderDetailInfo(@PathVariable String id){
        iWmsOrderDetailInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 订单详情修改
    * @return
    */
    @PostMapping("updWmsOrderDetailInfo")
    public AjaxResult updWmsOrderDetailInfo(@Valid @RequestBody WmsOrderDetailInfoForm form){
        WmsOrderDetailInfo wmsOrderDetailInfo = wmsOrderDetailInfoMapStructMapper.formToWmsOrderDetailInfo(form);
        iWmsOrderDetailInfoService.updateById(wmsOrderDetailInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得订单详情列表
     * @return 订单明细列表
     */
    @RequestMapping("findWmsOrderDetailInfoList/{orderNo}")
    public AjaxResult findWmsOrderDetailInfoList(@PathVariable String orderNo){
        List<WmsOrderDetailInfo> wmsOrderDetailInfos = iWmsOrderDetailInfoService.list(
                Wrappers.<WmsOrderDetailInfo>lambdaQuery().eq(WmsOrderDetailInfo::getOrderNo,orderNo)
        );
        return AjaxResult.success(wmsOrderDetailInfos);
    }

    /**
     * 获得订单详情详情通过ID
     * @return AjaxResult(WmsOrderDetailInfo)
     */
    @RequestMapping("findWmsOrderDetailInfoById/{id}")
    public AjaxResult findWmsOrderDetailInfoById(@PathVariable String id){
        WmsOrderDetailInfo wmsOrderDetailInfo = iWmsOrderDetailInfoService.getById(id);
        return AjaxResult.success(wmsOrderDetailInfo);
    }

    /**
     * 订单明细导出
     * @param response
     * @param search
     */
    @Log(title = "订单明细管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderSearch search)
    {
        List<WmsOrderDetailInfo> list = iWmsOrderDetailInfoService.list(
                Wrappers.<WmsOrderDetailInfo>lambdaQuery()
                        .eq(WmsOrderDetailInfo::getOrderNo,search.getOrderNo())
                        .orderByDesc(WmsOrderDetailInfo::getCreateDate)
        );
        ExcelUtil<WmsOrderDetailInfoExcel> util = new ExcelUtil<WmsOrderDetailInfoExcel>(WmsOrderDetailInfoExcel.class);
        util.exportExcel(response, wmsOrderDetailInfoMapStructMapper.infosToExcels(list), "订单明细数据");
    }
}

