package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 盘点
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsJudgeInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 判断计划名称
     */
   // @NotEmpty(message = "判断计划名称不能为空")
    private String judgeName;
        /**
     * 盘点类型 wms_judge_type A:全库 W:判断库位 P:判断产品 C:判断指定容器
     */
   // @NotEmpty(message = "盘点类型 wms_judge_type A:全库 W:判断库位 P:判断产品 C:判断指定容器不能为空")
    private String judgeType;
        /**
     * 0: 新加 10：发布（进行中）20：完成 99：关闭
     */
   // @NotEmpty(message = "0: 新加 10：发布（进行中）20：完成 99：关闭不能为空")
    private Integer status;
        /**
     * Y:执行 N:未执行 用盘点结果调平库存
     */
   // @NotEmpty(message = "Y:执行 N:未执行 用盘点结果调平库存不能为空")
    private String executeFlag;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
