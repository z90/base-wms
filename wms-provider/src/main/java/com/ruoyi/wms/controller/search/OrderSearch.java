package com.ruoyi.wms.controller.search;

import com.ruoyi.common.core.search.HlxBaseSearch;
import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-10
 * @Desc
 */
@Data
public class OrderSearch extends HlxBaseSearch {
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单名称
     */
    private String orderName;

    /**
     * 订单状态
     */
    private Integer orderStatus;
}
