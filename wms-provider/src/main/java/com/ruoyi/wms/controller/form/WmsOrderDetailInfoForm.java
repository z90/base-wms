package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 订单详情
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsOrderDetailInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 订单号
     */
   // @NotEmpty(message = "订单号不能为空")
    private String orderNo;
        /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
    private String productCode;
        /**
     * 产品名称
     */
   // @NotEmpty(message = "产品名称不能为空")
    private String productName;
        /**
     * 产品类型
     */
   // @NotEmpty(message = "产品类型不能为空")
    private String productType;
        /**
     * 单位 字典 wms_product_unit
     */
   // @NotEmpty(message = "单位 字典 wms_product_unit不能为空")
    private String productUnit;
        /**
     * 数量
     */
   // @NotEmpty(message = "数量不能为空")
    private BigDecimal num;
        /**
     * 状态 0：进行中 10：完成 99：关闭
     */
   // @NotEmpty(message = "状态 0：进行中 10：完成 99：关闭不能为空")
    private Integer status;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
