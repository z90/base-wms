package com.ruoyi.wms.controller.form;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 单位转换表
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsProductUnitConvertForm implements Serializable {


    /**
     * id
     */
    private String id;

    /**
     * 产品编码
     */
    @NotEmpty(message = "产品不存在")
    private String productCode;

    /**
     * 原单位
     */
    private String fromUnit;

    /**
     * 转换单位
     */
    private String toUnit;

     /**
     * 转换比
     */
    private BigDecimal convertNum;


}
