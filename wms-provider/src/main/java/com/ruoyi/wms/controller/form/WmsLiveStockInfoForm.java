package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 实时库存
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsLiveStockInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
    private String productCode;
        /**
     * 产品名称
     */
   // @NotEmpty(message = "产品名称不能为空")
    private String productName;
        /**
     * 单位 字典 wms_product_unit
     */
   // @NotEmpty(message = "单位 字典 wms_product_unit不能为空")
    private String productUnit;
        /**
     * 产品类型
     */
   // @NotEmpty(message = "产品类型不能为空")
    private String productType;
        /**
     * 容器编码
     */
   // @NotEmpty(message = "容器编码不能为空")
    private String containerCode;
        /**
     * 数量
     */
   // @NotEmpty(message = "数量不能为空")
    private BigDecimal num;
        /**
     * 仓库编码
     */
   // @NotEmpty(message = "仓库编码不能为空")
    private String warehouseCode;
        /**
     * 库位编码
     */
   // @NotEmpty(message = "库位编码不能为空")
    private String locationCode;
        /**
     * 供应商编码
     */
   // @NotEmpty(message = "供应商编码不能为空")
    private String supplierCode;
        /**
     * 是否锁定：N:否 Y:是
     */
   // @NotEmpty(message = "是否锁定：N:否 Y:是不能为空")
    private String lockFlag;

        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
