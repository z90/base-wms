package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsJudgePlanInfoForm;
import com.ruoyi.wms.mapstruct.WmsJudgePlanInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsJudgePlanInfoService;
import com.ruoyi.wms.entity.WmsJudgePlanInfo;
import javax.validation.Valid;


/**
 * <p>
 * 盘点计划信息 前端控制器实体类（WmsJudgePlanInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsJudgePlanInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsJudgePlanInfoController extends BaseController {
    private final IWmsJudgePlanInfoService iWmsJudgePlanInfoService;
    private final WmsJudgePlanInfoMapStructMapper wmsJudgePlanInfoMapStructMapper;

    /**
     * 新增盘点计划信息
     * @param form
     * @return
     */
    @PostMapping("saveWmsJudgePlanInfo")
    public AjaxResult saveWmsJudgePlanInfo(@Valid @RequestBody WmsJudgePlanInfoForm form){
        WmsJudgePlanInfo wmsJudgePlanInfo = wmsJudgePlanInfoMapStructMapper.formToWmsJudgePlanInfo(form);
        iWmsJudgePlanInfoService.save(wmsJudgePlanInfo);
        return AjaxResult.success();
    }
    /**
     * 盘点计划信息删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsJudgePlanInfo(@PathVariable String id){
        iWmsJudgePlanInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 盘点计划信息修改
    * @return
    */
    @PostMapping("updWmsJudgePlanInfo")
    public AjaxResult updWmsJudgePlanInfo(@Valid @RequestBody WmsJudgePlanInfoForm form){
        WmsJudgePlanInfo wmsJudgePlanInfo = wmsJudgePlanInfoMapStructMapper.formToWmsJudgePlanInfo(form);
        iWmsJudgePlanInfoService.updateById(wmsJudgePlanInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得盘点计划信息列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsJudgePlanInfoList")
    public AjaxResult findWmsJudgePlanInfoList(Page<WmsJudgePlanInfo> page){
        iWmsJudgePlanInfoService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得盘点计划信息详情通过ID
     * @return AjaxResult(WmsJudgePlanInfo)
     */
    @RequestMapping("findWmsJudgePlanInfoById")
    public AjaxResult findWmsJudgePlanInfoById(@PathVariable String id){
        WmsJudgePlanInfo wmsJudgePlanInfo = iWmsJudgePlanInfoService.getById(id);
        return AjaxResult.success(wmsJudgePlanInfo);
    }
}

