package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsInLogForm;
import com.ruoyi.wms.mapstruct.WmsInLogMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsInLogService;
import com.ruoyi.wms.entity.WmsInLog;
import javax.validation.Valid;

/**
 * <p>
 * 入库记录 前端控制器实体类（WmsInLog）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsInLog")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsInLogController extends BaseController {
    private final IWmsInLogService iWmsInLogService;
    private final WmsInLogMapStructMapper wmsInLogMapStructMapper;

    /**
     * 新增入库记录
     * @param form
     * @return
     */
    @PostMapping("saveWmsInLog")
    public AjaxResult saveWmsInLog(@Valid @RequestBody WmsInLogForm form){
        WmsInLog wmsInLog = wmsInLogMapStructMapper.formToWmsInLog(form);
        iWmsInLogService.save(wmsInLog);
        return AjaxResult.success();
    }
    /**
     * 入库记录删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsInLog(@PathVariable String id){
        iWmsInLogService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 入库记录修改
    * @return
    */
    @PostMapping("updWmsInLog")
    public AjaxResult updWmsInLog(@Valid @RequestBody WmsInLogForm form){
        WmsInLog wmsInLog = wmsInLogMapStructMapper.formToWmsInLog(form);
        iWmsInLogService.updateById(wmsInLog);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得入库记录列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsInLogList")
    public AjaxResult findWmsInLogList(Page<WmsInLog> page){
        iWmsInLogService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得入库记录详情通过ID
     * @return AjaxResult(WmsInLog)
     */
    @RequestMapping("findWmsInLogById")
    public AjaxResult findWmsInLogById(@PathVariable String id){
        WmsInLog wmsInLog = iWmsInLogService.getById(id);
        return AjaxResult.success(wmsInLog);
    }
}

