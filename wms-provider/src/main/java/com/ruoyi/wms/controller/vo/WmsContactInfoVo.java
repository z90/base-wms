package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * <p>
 * 供应商联系人
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsContactInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * id 供应商 or 客户id
     */
    private String sourceId;
    /**
     * 联系人
     */
    private String userName;
    /**
     * 联系电话
     */
    private String userTel;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 地址
     */
    private String address;

}
