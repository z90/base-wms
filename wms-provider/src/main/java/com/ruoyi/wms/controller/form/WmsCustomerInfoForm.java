package com.ruoyi.wms.controller.form;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 客户信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsCustomerInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;

              /**
     * 客户编码
     */
   // @NotEmpty(message = "客户编码不能为空")
    private String customerCode;
        /**
     * 客户名称
     */
   // @NotEmpty(message = "客户名称不能为空")
    private String customerName;
        /**
     * 状态 Y: 启用 N:禁用
     */
   // @NotEmpty(message = "状态 Y: 启用 N:禁用不能为空")
    private String customerStatus;
        /**
     * 地址
     */
   // @NotEmpty(message = "地址不能为空")
    private String address;
        /**
     * 邮编
     */
   // @NotEmpty(message = "邮编不能为空")
    private String postcode;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

    /**
     * 传真
     */
    private String fax;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 类型
     */
    private String customerType;
    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;
  }
