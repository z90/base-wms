package com.ruoyi.wms.controller.excel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.WmsBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 客户信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsCustomerInfoExcel implements Serializable {


    /**
     * 客户编码
     */
    @Excel(name = "客户编码")
    private String customerCode;

    /**
     * 客户名称
     */
    @Excel(name = "客户名称")
    private String customerName;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;

    /**
     * 电话
     */
    @Excel(name = "联系人")
    private String userName;
    /**
     * 电话
     */
    @Excel(name = "电话")
    private String userTel;

    /**
     * 传真
     */
    @Excel(name = "传真")
    private String fax;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String email;


}
