package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 盘点
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsJudgeInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 判断计划名称
     */
    private String judgeName;
    /**
     * 盘点类型 wms_judge_type A:全库 W:判断库位 P:判断产品 C:判断指定容器
     */
    private String judgeType;
    /**
     * 0: 新加 10：发布（进行中）20：完成 99：关闭
     */
    private Integer status;
    /**
     * Y:执行 N:未执行 用盘点结果调平库存
     */
    private String executeFlag;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
}
