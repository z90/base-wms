package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wms.controller.form.WmsPurchaseOrderInfoForm;
import com.ruoyi.wms.controller.search.OrderSearch;
import com.ruoyi.wms.mapstruct.WmsPurchaseOrderInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsPurchaseOrderInfoService;



import com.ruoyi.wms.entity.WmsPurchaseOrderInfo;
import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 采购订单 前端控制器实体类（WmsPurchaseOrderInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsPurchaseOrderInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsPurchaseOrderInfoController extends BaseController {
    private final IWmsPurchaseOrderInfoService iWmsPurchaseOrderInfoService;
    private final WmsPurchaseOrderInfoMapStructMapper wmsPurchaseOrderInfoMapStructMapper;

    /**
     * 新增采购订单
     * @param form
     * @return
     */
    @PostMapping("saveWmsPurchaseOrderInfo")
    public AjaxResult saveWmsPurchaseOrderInfo(@Valid @RequestBody WmsPurchaseOrderInfoForm form){
        iWmsPurchaseOrderInfoService.insertOrderInfo(form);
        return AjaxResult.success();
    }

    /**
     * 取消订单
     * @param id 订单id
     * @return
     */
    @GetMapping("/cancelOrder/{id}")
    public AjaxResult cancelOrder(@PathVariable String id){
        iWmsPurchaseOrderInfoService.cancelOrderById(id);
        return AjaxResult.success("取消成功");
    }

    /**
     * 重启订单
     * @param id
     * @return
     */
    @GetMapping("/restartOrder/{id}")
    public AjaxResult restartOrder(@PathVariable String id){
        iWmsPurchaseOrderInfoService.restartOrderById(id);
        return AjaxResult.success("重启成功");
    }

    /**
     * 采购订单删除
     * @param form
     * @return
     */
    @DeleteMapping("/delWmsPurchaseOrderInfo/{id}")
    public AjaxResult delWmsPurchaseOrderInfo(@PathVariable String id){
        iWmsPurchaseOrderInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 采购订单修改
    * @return
    */
    @PostMapping("updWmsPurchaseOrderInfo")
    public AjaxResult updWmsPurchaseOrderInfo(@Valid @RequestBody WmsPurchaseOrderInfoForm form){
        WmsPurchaseOrderInfo wmsPurchaseOrderInfo = wmsPurchaseOrderInfoMapStructMapper.formToWmsPurchaseOrderInfo(form);
        iWmsPurchaseOrderInfoService.updateById(wmsPurchaseOrderInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得采购订单列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsPurchaseOrderInfoList")
    public TableDataInfo findWmsPurchaseOrderInfoList(OrderSearch search){
        startPage();
        List<WmsPurchaseOrderInfo> list = iWmsPurchaseOrderInfoService.list(
                Wrappers.<WmsPurchaseOrderInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getOrderName()),WmsPurchaseOrderInfo::getOrderName,search.getOrderName())
                        .like(StrUtil.isNotEmpty(search.getOrderNo()),WmsPurchaseOrderInfo::getOrderNo,search.getOrderNo())
                        .like(StrUtil.isNotEmpty(search.getKeyword()), WmsPurchaseOrderInfo::getSupplierName,search.getKeyword())
                        .eq(search.getOrderStatus()!=null,WmsPurchaseOrderInfo::getStatus,search.getOrderStatus())
                        .orderByDesc(WmsPurchaseOrderInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得采购订单详情通过ID
     * @return AjaxResult(WmsPurchaseOrderInfo)
     */
    @RequestMapping("findWmsPurchaseOrderInfoById/{id}")
    public AjaxResult findWmsPurchaseOrderInfoById(@PathVariable String id){
        WmsPurchaseOrderInfo wmsPurchaseOrderInfo = iWmsPurchaseOrderInfoService.getById(id);
        
        return AjaxResult.success(wmsPurchaseOrderInfo);
    }

    /**
     * 通过订单号获得订单信息
     * @param orderNo 订单号
     * @return 订单信息
     */
    @RequestMapping("findWmsPurchaseOrderInfoByOrderNo/{orderNo}")
    public AjaxResult findWmsPurchaseOrderInfoByOrderNo(@PathVariable String orderNo){
        WmsPurchaseOrderInfo wmsPurchaseOrderInfo = iWmsPurchaseOrderInfoService.getByOrderNo(orderNo);
        return AjaxResult.success(wmsPurchaseOrderInfo);
    }
}

