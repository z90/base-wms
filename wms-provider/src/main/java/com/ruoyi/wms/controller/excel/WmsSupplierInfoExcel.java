package com.ruoyi.wms.controller.excel;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 供应商
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsSupplierInfoExcel implements Serializable {


    /**
     * 供应商编码
     */
    @Excel(name = "供应商编号")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @Excel(name = "供应商名称")
    private String supplierName;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;
    /**
     * 传真
     */
    @Excel(name = "传真")
    private String fax;

    /**
     * 邮箱
     */
    @Excel(name = "Email")
    private String email;

    /**
     * 类型
     */
    @Excel(name = "类型",type = Excel.Type.IMPORT)
    private String supplierType;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String userName;

    /**
     * 联系电话
     */
    @Excel(name = "电话")
    private String userTel;

}
