package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsJudgeInfoForm;
import com.ruoyi.wms.mapstruct.WmsJudgeInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsJudgeInfoService;
import com.ruoyi.wms.entity.WmsJudgeInfo;
import javax.validation.Valid;


/**
 * <p>
 * 盘点 前端控制器实体类（WmsJudgeInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsJudgeInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsJudgeInfoController extends BaseController {
    private final IWmsJudgeInfoService iWmsJudgeInfoService;
    private final WmsJudgeInfoMapStructMapper wmsJudgeInfoMapStructMapper;

    /**
     * 新增盘点
     * @param form
     * @return
     */
    @PostMapping("saveWmsJudgeInfo")
    public AjaxResult saveWmsJudgeInfo(@Valid @RequestBody WmsJudgeInfoForm form){
        WmsJudgeInfo wmsJudgeInfo = wmsJudgeInfoMapStructMapper.formToWmsJudgeInfo(form);
        iWmsJudgeInfoService.save(wmsJudgeInfo);
        return AjaxResult.success();
    }
    /**
     * 盘点删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsJudgeInfo(@PathVariable String id){
        iWmsJudgeInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 盘点修改
    * @return
    */
    @PostMapping("updWmsJudgeInfo")
    public AjaxResult updWmsJudgeInfo(@Valid @RequestBody WmsJudgeInfoForm form){
        WmsJudgeInfo wmsJudgeInfo = wmsJudgeInfoMapStructMapper.formToWmsJudgeInfo(form);
        iWmsJudgeInfoService.updateById(wmsJudgeInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得盘点列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsJudgeInfoList")
    public AjaxResult findWmsJudgeInfoList(Page<WmsJudgeInfo> page){
        iWmsJudgeInfoService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得盘点详情通过ID
     * @return AjaxResult(WmsJudgeInfo)
     */
    @RequestMapping("findWmsJudgeInfoById")
    public AjaxResult findWmsJudgeInfoById(@PathVariable String id){
        WmsJudgeInfo wmsJudgeInfo = iWmsJudgeInfoService.getById(id);
        return AjaxResult.success(wmsJudgeInfo);
    }
}

