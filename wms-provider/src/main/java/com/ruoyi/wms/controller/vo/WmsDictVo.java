package com.ruoyi.wms.controller.vo;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-08-18
 * @Desc
 */
@Data
public class WmsDictVo {
    /**
     * 标题
     */
    private String label;

    /**
     * 值
     */
    private String value;

    /**
     * 编码
     */
    private String code;

    /**
     * 样式控制
     */
    private WmsDictRawVo raw = new WmsDictRawVo();
}
