package com.ruoyi.wms.controller.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Data
public class CustomerSearch {
    private String customerCode;
    private String customerName;

    private String userTel;
}
