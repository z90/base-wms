package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsLiveStockInfoForm;
import com.ruoyi.wms.mapstruct.WmsLiveStockInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsLiveStockInfoService;
import com.ruoyi.wms.entity.WmsLiveStockInfo;
import javax.validation.Valid;

/**
 * <p>
 * 实时库存 前端控制器实体类（WmsLiveStockInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsLiveStockInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsLiveStockInfoController extends BaseController {
    private final IWmsLiveStockInfoService iWmsLiveStockInfoService;
    private final WmsLiveStockInfoMapStructMapper wmsLiveStockInfoMapStructMapper;

    /**
     * 新增实时库存
     * @param form
     * @return
     */
    @PostMapping("saveWmsLiveStockInfo")
    public AjaxResult saveWmsLiveStockInfo(@Valid @RequestBody WmsLiveStockInfoForm form){
        WmsLiveStockInfo wmsLiveStockInfo = wmsLiveStockInfoMapStructMapper.formToWmsLiveStockInfo(form);
        iWmsLiveStockInfoService.save(wmsLiveStockInfo);
        return AjaxResult.success();
    }
    /**
     * 实时库存删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsLiveStockInfo(@PathVariable String id){
        iWmsLiveStockInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 实时库存修改
    * @return
    */
    @PostMapping("updWmsLiveStockInfo")
    public AjaxResult updWmsLiveStockInfo(@Valid @RequestBody WmsLiveStockInfoForm form){
        WmsLiveStockInfo wmsLiveStockInfo = wmsLiveStockInfoMapStructMapper.formToWmsLiveStockInfo(form);
        iWmsLiveStockInfoService.updateById(wmsLiveStockInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得实时库存列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsLiveStockInfoList")
    public AjaxResult findWmsLiveStockInfoList(Page<WmsLiveStockInfo> page){
        iWmsLiveStockInfoService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得实时库存详情通过ID
     * @return AjaxResult(WmsLiveStockInfo)
     */
    @RequestMapping("findWmsLiveStockInfoById")
    public AjaxResult findWmsLiveStockInfoById(@PathVariable String id){
        WmsLiveStockInfo wmsLiveStockInfo = iWmsLiveStockInfoService.getById(id);
        return AjaxResult.success(wmsLiveStockInfo);
    }
}

