package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 出库记录
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsOutLogForm implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long id;
  /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
    private String productCode;
        /**
     * 产品名称
     */
   // @NotEmpty(message = "产品名称不能为空")
    private String productName;
        /**
     * 单位 字典 wms_product_unit
     */
   // @NotEmpty(message = "单位 字典 wms_product_unit不能为空")
    private String productUnit;
        /**
     * 产品类型
     */
   // @NotEmpty(message = "产品类型不能为空")
    private String productType;
        /**
     * 业务编码
     */
   // @NotEmpty(message = "业务编码不能为空")
    private String businessCode;
        /**
     * 容器编码
     */
   // @NotEmpty(message = "容器编码不能为空")
    private String containerCode;
        /**
     * 数量
     */
   // @NotEmpty(message = "数量不能为空")
    private BigDecimal num;
        /**
     * 类型 wms_out_action
     */
   // @NotEmpty(message = "类型 wms_out_action不能为空")
    private String actionType;
        /**
     * 来源 库位
     */
   // @NotEmpty(message = "来源 库位不能为空")
    private String sourceCode;
        /**
     * 来源名称
     */
   // @NotEmpty(message = "来源名称不能为空")
    private String sourceName;
        /**
     * 目标库位 库位 or 客户
     */
   // @NotEmpty(message = "目标库位 库位 or 客户不能为空")
    private String targetCode;
        /**
     * 目标名称
     */
   // @NotEmpty(message = "目标名称不能为空")
    private String targetName;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
