package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wms.controller.form.WmsWarehouseInfoForm;
import com.ruoyi.wms.controller.search.WarehouseSearch;
import com.ruoyi.wms.mapstruct.WmsDictMapStructMapper;
import com.ruoyi.wms.mapstruct.WmsWarehouseInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.wms.service.IWmsWarehouseInfoService;
import com.ruoyi.wms.entity.WmsWarehouseInfo;
import javax.validation.Valid;
import java.util.List;

 /**
 * 仓库管理 前端控制器实体类（WmsWarehouseInfo）
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsWarehouseInfo")
@AllArgsConstructor
public class WmsWarehouseInfoController extends BaseController {
    private final IWmsWarehouseInfoService iWmsWarehouseInfoService;
    private final WmsWarehouseInfoMapStructMapper wmsWarehouseInfoMapStructMapper;
    private final WmsDictMapStructMapper wmsDictMapStructMapper;

    /**
     * 新增仓库管理
     * @param form
     * @return
     */
    @PostMapping("saveWmsWarehouseInfo")
    public AjaxResult saveWmsWarehouseInfo(@Valid @RequestBody WmsWarehouseInfoForm form){
        WmsWarehouseInfo wmsWarehouseInfo = wmsWarehouseInfoMapStructMapper.formToWmsWarehouseInfo(form);
        WmsWarehouseInfo wmsWarehouseInfo1 = iWmsWarehouseInfoService.getInfoByCode(form.getWarehouseCode());
        Assert.isNull(wmsWarehouseInfo1,"仓库编码已存在！");
        iWmsWarehouseInfoService.save(wmsWarehouseInfo);
        return AjaxResult.success();
    }
    /**
     * 仓库管理删除
     * @param form
     * @return
     */
    @DeleteMapping("/delWmsWarehouseInfo/{id}")
    public AjaxResult delWmsWarehouseInfo(@PathVariable String id){
        iWmsWarehouseInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 仓库管理修改
    * @return
    */
    @PutMapping("updWmsWarehouseInfo")
    public AjaxResult updWmsWarehouseInfo(@Valid @RequestBody WmsWarehouseInfoForm form){
        WmsWarehouseInfo wmsWarehouseInfo = wmsWarehouseInfoMapStructMapper.formToWmsWarehouseInfo(form);
        iWmsWarehouseInfoService.updateById(wmsWarehouseInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得仓库管理列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsWarehouseInfoList")
    public TableDataInfo findWmsWarehouseInfoList(WarehouseSearch search){
        startPage();
        List<WmsWarehouseInfo> list = iWmsWarehouseInfoService.list(
                Wrappers.<WmsWarehouseInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getWarehouseName()),WmsWarehouseInfo::getWarehouseName,search.getWarehouseName())
                        .like(StrUtil.isNotEmpty(search.getWarehouseCode()),WmsWarehouseInfo::getWarehouseCode,search.getWarehouseCode())
                        .eq(StrUtil.isNotEmpty(search.getWarehouseStatus()),WmsWarehouseInfo::getWarehouseStatus,search.getWarehouseStatus())
                        .orderByDesc(WmsWarehouseInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得仓库管理详情通过ID
     * @return AjaxResult(WmsWarehouseInfo)
     */
    @RequestMapping("/findWmsWarehouseInfoById/{id}")
    public AjaxResult findWmsWarehouseInfoById(@PathVariable String id){
        WmsWarehouseInfo wmsWarehouseInfo = iWmsWarehouseInfoService.getInfoById(id);
        
        return AjaxResult.success(wmsWarehouseInfo);
    }

    /**
     * 获得仓库列表
     * @return 仓库列表
     */
    @GetMapping("/getDictList")
    public AjaxResult getDictList(){
        List<WmsWarehouseInfo> list = iWmsWarehouseInfoService.list(
                Wrappers.<WmsWarehouseInfo>lambdaQuery().orderByAsc(WmsWarehouseInfo::getOrderBy).orderByDesc(WmsWarehouseInfo::getCreateDate)
        );
        return AjaxResult.success(wmsDictMapStructMapper.warehousesToDicts(list));
    }
}

