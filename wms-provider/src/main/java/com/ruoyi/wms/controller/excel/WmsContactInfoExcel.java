package com.ruoyi.wms.controller.excel;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 供应商联系人
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsContactInfoExcel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String userName;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;

    /**
     * 联系电话
     */
    @Excel(name = "电话")
    private String userTel;



    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private LocalDateTime createDate;


}
