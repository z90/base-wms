package com.ruoyi.wms.controller.excel;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsProductInfoExcel implements Serializable {

  private static final long serialVersionUID = 1L;

              /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
   @Excel(name = "产品编码")
   private String productCode;
        /**
     * 产品名称
     */
   // @NotEmpty(message = "产品名称不能为空")
    @Excel(name = "产品名称")
    private String productName;
        /**
     * 产品类型
     */
   // @NotEmpty(message = "产品类型不能为空")
    @Excel(name = "产品类型", type = Excel.Type.IMPORT)
    private String productType;

    /**
     * 规格
     */
    @Excel(name = "规格")
    private String specUnit;
        /**
     * 单位 字典 wms_product_unit
     */
   // @NotEmpty(message = "单位 字典 wms_product_unit不能为空")
    @Excel(name = "单位")
    private String productUnit;
        /**
     * 最小库存
     */
   // @NotEmpty(message = "最小库存不能为空")
    @Excel(name = "预警(下)", cellType = Excel.ColumnType.NUMERIC)
    private BigDecimal minStock;
        /**
     * 最大库存
     */
   // @NotEmpty(message = "最大库存不能为空")
    @Excel(name = "预警(上)", cellType = Excel.ColumnType.NUMERIC)
    private BigDecimal maxStock;


  }
