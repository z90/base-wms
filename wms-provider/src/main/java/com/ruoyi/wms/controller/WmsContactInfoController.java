package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wms.controller.excel.WmsContactInfoExcel;
import com.ruoyi.wms.controller.form.WmsContactInfoForm;
import com.ruoyi.wms.controller.search.ContactSearch;
import com.ruoyi.wms.mapstruct.WmsContactInfoMapStructMapper;
import com.ruoyi.wms.mapstruct.WmsDictMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsContactInfoService;
import com.ruoyi.wms.entity.WmsContactInfo;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 供应商联系人 前端控制器实体类（WmsContactInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsContactInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsContactInfoController extends BaseController {
    private final IWmsContactInfoService iWmsContactInfoService;
    private final WmsContactInfoMapStructMapper wmsContactInfoMapStructMapper;
    private final WmsDictMapStructMapper wmsDictMapStructMapper;

    /**
     * 新增供应商联系人
     * @param form
     * @return
     */
    @PostMapping("saveWmsContactInfo")
    public AjaxResult saveWmsContactInfo(@Valid @RequestBody WmsContactInfoForm form){
        WmsContactInfo wmsContactInfo = wmsContactInfoMapStructMapper.formToWmsContactInfo(form);
        iWmsContactInfoService.save(wmsContactInfo);
        return AjaxResult.success(wmsContactInfo);
    }
    /**
     * 供应商联系人删除
     * @param form
     * @return
     */
    @DeleteMapping("/delWmsContactInfo/{id}")
    public AjaxResult delWmsContactInfo(@PathVariable String id){
        iWmsContactInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 供应商联系人修改
    * @return
    */
    @PutMapping("updWmsContactInfo")
    public AjaxResult updWmsContactInfo(@Valid @RequestBody WmsContactInfoForm form){
        WmsContactInfo wmsContactInfo = wmsContactInfoMapStructMapper.formToWmsContactInfo(form);
        iWmsContactInfoService.updateById(wmsContactInfo);
        return AjaxResult.success("修改成功");
    }


    /**
     * 获得供应商联系人详情通过ID
     * @return Result(WmsContactInfo)
     */
    @GetMapping("findWmsContactInfoById/{id}")
    public AjaxResult findWmsContactInfoById(@PathVariable String id){
        WmsContactInfo wmsContactInfo = iWmsContactInfoService.getById(id);
        return AjaxResult.success(wmsContactInfo);
    }

    /**
     * 获得联系人列表
     */
    @GetMapping("/getContactList")
    public AjaxResult getContactList(@Valid ContactSearch search){

        List<WmsContactInfo> list = iWmsContactInfoService.list(
                Wrappers.<WmsContactInfo>lambdaQuery().eq(WmsContactInfo::getSourceId,search.getSourceId())
        );
        return AjaxResult.success(wmsContactInfoMapStructMapper.wmsContactInfoToVo(list));
    }

    /**
     * 联系人导出
     * @param response
     * @param search
     */
    @Log(title = "联系人管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ContactSearch search)
    {
        List<WmsContactInfo> list = iWmsContactInfoService.list(
                Wrappers.<WmsContactInfo>lambdaQuery().eq(WmsContactInfo::getSourceId,search.getSourceId())
        );
        ExcelUtil<WmsContactInfoExcel> util = new ExcelUtil<WmsContactInfoExcel>(WmsContactInfoExcel.class);
        util.exportExcel(response, wmsContactInfoMapStructMapper.infosToExcels(list), "联系人数据");
    }

    /**
     * 获得联系方式字典数据
     * @return
     */
    @GetMapping("/getDictList/{code}")
    public AjaxResult getDictList(@PathVariable String code){
        List<WmsContactInfo> list = iWmsContactInfoService.list(
                Wrappers.<WmsContactInfo>lambdaQuery().eq(WmsContactInfo::getSourceId,code)
        );
        return AjaxResult.success(wmsDictMapStructMapper.contactsToDicts(list));
    }
}

