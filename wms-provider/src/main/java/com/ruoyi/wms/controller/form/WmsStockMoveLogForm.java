package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 移库记录
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsStockMoveLogForm implements Serializable {

  private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
              /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
    private String productCode;
        /**
     * 原库
     */
   // @NotEmpty(message = "原库不能为空")
    private String sourceCode;
        /**
     * 目标库
     */
   // @NotEmpty(message = "目标库不能为空")
    private String targetCode;
        /**
     * 容器编码
     */
   // @NotEmpty(message = "容器编码不能为空")
    private String containerCode;
        /**
     * 数量
     */
   // @NotEmpty(message = "数量不能为空")
    private BigDecimal num;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
