package com.ruoyi.wms.controller.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Data
public class SupplierSearch {

    private String supplierCode;
    private String supplierName;
    private String supplierType;
    private String userTel;
}
