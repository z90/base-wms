package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsStockMoveLogForm;
import com.ruoyi.wms.mapstruct.WmsStockMoveLogMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsStockMoveLogService;



import com.ruoyi.wms.entity.WmsStockMoveLog;
import javax.validation.Valid;




/**
 * <p>
 * 移库记录 前端控制器实体类（WmsStockMoveLog）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsStockMoveLog")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsStockMoveLogController extends BaseController {
    private final IWmsStockMoveLogService iWmsStockMoveLogService;
    private final WmsStockMoveLogMapStructMapper wmsStockMoveLogMapStructMapper;

    /**
     * 新增移库记录
     * @param form
     * @return
     */
    @PostMapping("saveWmsStockMoveLog")
    public AjaxResult saveWmsStockMoveLog(@Valid @RequestBody WmsStockMoveLogForm form){
        WmsStockMoveLog wmsStockMoveLog = wmsStockMoveLogMapStructMapper.formToWmsStockMoveLog(form);
        iWmsStockMoveLogService.save(wmsStockMoveLog);
        return AjaxResult.success();
    }
    /**
     * 移库记录删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsStockMoveLog(@PathVariable String id){
        iWmsStockMoveLogService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 移库记录修改
    * @return
    */
    @PostMapping("updWmsStockMoveLog")
    public AjaxResult updWmsStockMoveLog(@Valid @RequestBody WmsStockMoveLogForm form){
        WmsStockMoveLog wmsStockMoveLog = wmsStockMoveLogMapStructMapper.formToWmsStockMoveLog(form);
        iWmsStockMoveLogService.updateById(wmsStockMoveLog);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得移库记录列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsStockMoveLogList")
    public AjaxResult findWmsStockMoveLogList(Page<WmsStockMoveLog> page){
        iWmsStockMoveLogService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得移库记录详情通过ID
     * @return AjaxResult(WmsStockMoveLog)
     */
    @RequestMapping("findWmsStockMoveLogById")
    public AjaxResult findWmsStockMoveLogById(@PathVariable String id){
        WmsStockMoveLog wmsStockMoveLog = iWmsStockMoveLogService.getById(id);
        
        return AjaxResult.success(wmsStockMoveLog);
    }
}

