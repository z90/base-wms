package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsFileInfoForm;
import com.ruoyi.wms.mapstruct.WmsFileInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsFileInfoService;
import com.ruoyi.wms.entity.WmsFileInfo;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器实体类（WmsFileInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsFileInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsFileInfoController extends BaseController {
    private final IWmsFileInfoService iWmsFileInfoService;
    private final WmsFileInfoMapStructMapper wmsFileInfoMapStructMapper;

    /**
     * 新增
     * @param form
     * @return
     */
    @PostMapping("saveWmsFileInfo")
    public AjaxResult saveWmsFileInfo(@Valid @RequestBody WmsFileInfoForm form){
        WmsFileInfo wmsFileInfo = wmsFileInfoMapStructMapper.formToWmsFileInfo(form);
        iWmsFileInfoService.save(wmsFileInfo);
        return AjaxResult.success();
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsFileInfo(@PathVariable String id){
        iWmsFileInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 修改
    * @return
    */
    @PostMapping("updWmsFileInfo")
    public AjaxResult updWmsFileInfo(@Valid @RequestBody WmsFileInfoForm form){
        WmsFileInfo wmsFileInfo = wmsFileInfoMapStructMapper.formToWmsFileInfo(form);
        iWmsFileInfoService.updateById(wmsFileInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsFileInfoList")
    public AjaxResult findWmsFileInfoList(Page<WmsFileInfo> page){
        iWmsFileInfoService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得详情通过ID
     * @return AjaxResult(WmsFileInfo)
     */
    @RequestMapping("findWmsFileInfoById/{id}")
    public AjaxResult findWmsFileInfoById(@PathVariable String id){
        WmsFileInfo wmsFileInfo = iWmsFileInfoService.getById(id);
        return AjaxResult.success(wmsFileInfo);
    }
}

