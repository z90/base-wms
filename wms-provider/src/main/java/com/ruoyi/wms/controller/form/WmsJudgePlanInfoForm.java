package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 盘点计划信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsJudgePlanInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 盘点id
     */
   // @NotEmpty(message = "盘点id不能为空")
    private String judgeId;
        /**
     * 盘点内容编码
     */
   // @NotEmpty(message = "盘点内容编码不能为空")
    private String judgeCode;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
