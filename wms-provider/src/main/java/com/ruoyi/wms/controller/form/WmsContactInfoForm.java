package com.ruoyi.wms.controller.form;
import java.time.LocalDateTime;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
/**
 * <p>
 * 供应商联系人
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsContactInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
  private String id;
  /**
     * id 供应商 or 客户id
     */
    @NotEmpty(message = "数据不能为空")
    private String sourceId;
        /**
     * 联系人
     */
    @NotEmpty(message = "联系人不能为空")
    private String userName;
        /**
     * 联系电话
     */
    @NotEmpty(message = "联系电话不能为空")
    @Pattern(regexp = "^1(\\d{10})$",message = "电话格式错误")
    private String userTel;
        /**
     * 职务 字典：wms_contact_job
     */
   // @NotEmpty(message = "职务 字典：wms_contact_job不能为空")
    private String dutyCode;
        /**
     * 状态 Y: 启用 N:禁用
     */
   // @NotEmpty(message = "状态 Y: 启用 N:禁用不能为空")
    private String status;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

    /**
     * 地址
     */
    private String address;

  }
