package com.ruoyi.wms.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wms.controller.excel.WmsCustomerInfoExcel;
import com.ruoyi.wms.controller.form.WmsCustomerInfoForm;
import com.ruoyi.wms.controller.search.CustomerSearch;
import com.ruoyi.wms.mapstruct.WmsCustomerInfoMapStructMapper;
import com.ruoyi.wms.mapstruct.WmsDictMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsCustomerInfoService;
import com.ruoyi.wms.entity.WmsCustomerInfo;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 客户信息 前端控制器实体类（WmsCustomerInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsCustomerInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsCustomerInfoController extends BaseController {
    private final IWmsCustomerInfoService iWmsCustomerInfoService;
    private final WmsCustomerInfoMapStructMapper wmsCustomerInfoMapStructMapper;
    private final WmsDictMapStructMapper wmsDictMapStructMapper;

    /**
     * 新增客户信息
     * @param form
     * @return
     */
    @PostMapping("saveWmsCustomerInfo")
    public AjaxResult saveWmsCustomerInfo(@Valid @RequestBody WmsCustomerInfoForm form){
        WmsCustomerInfo wmsCustomerInfo = wmsCustomerInfoMapStructMapper.formToWmsCustomerInfo(form);
        WmsCustomerInfo info = iWmsCustomerInfoService.findByCode(form.getCustomerCode());
        if(!BeanUtil.isEmpty(info)){
            return AjaxResult.error("客户编码已存在");
        }
        iWmsCustomerInfoService.save(wmsCustomerInfo);
        return AjaxResult.success();
    }
    /**
     * 客户信息删除
     * @param form
     * @return
     */
    @DeleteMapping("/delWmsCustomerInfo/{id}")
    public AjaxResult delWmsCustomerInfo(@PathVariable String id){
        iWmsCustomerInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 客户信息修改
    * @return
    */
    @PutMapping("updWmsCustomerInfo")
    public AjaxResult updWmsCustomerInfo(@Valid @RequestBody WmsCustomerInfoForm form){
        WmsCustomerInfo wmsCustomerInfo = wmsCustomerInfoMapStructMapper.formToWmsCustomerInfo(form);
        WmsCustomerInfo info = iWmsCustomerInfoService.findByCode(form.getCustomerCode());
        if(!BeanUtil.isEmpty(info) && !info.getId().equals(form.getId())){
            return AjaxResult.error("客户编码已存在");
        }
        iWmsCustomerInfoService.updateById(wmsCustomerInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得客户信息列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsCustomerInfoList")
    public TableDataInfo findWmsCustomerInfoList(CustomerSearch search){
        startPage();
        List<WmsCustomerInfo> list = iWmsCustomerInfoService.list(
                Wrappers.<WmsCustomerInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getCustomerCode()),WmsCustomerInfo::getCustomerCode,search.getCustomerCode())
                        .like(StrUtil.isNotEmpty(search.getCustomerName()),WmsCustomerInfo::getCustomerName,search.getCustomerName())
                        .like(StrUtil.isNotEmpty(search.getUserTel()),WmsCustomerInfo::getUserTel,search.getUserTel())
                        .orderByDesc(WmsCustomerInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得客户信息详情通过ID
     * @return Result(WmsCustomerInfo)
     */
    @RequestMapping("findWmsCustomerInfoById/{id}")
    public AjaxResult findWmsCustomerInfoById(@PathVariable String id){
        WmsCustomerInfo wmsCustomerInfo = iWmsCustomerInfoService.getById(id);
        return AjaxResult.success(wmsCustomerInfo);
    }

    /**
     * 客户数据导出
     * @param response
     * @param search
     */
    @Log(title = "客户管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CustomerSearch search)
    {
        List<WmsCustomerInfo> list = iWmsCustomerInfoService.list(
                Wrappers.<WmsCustomerInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getCustomerCode()),WmsCustomerInfo::getCustomerCode,search.getCustomerCode())
                        .like(StrUtil.isNotEmpty(search.getCustomerName()),WmsCustomerInfo::getCustomerName,search.getCustomerName())
                        .like(StrUtil.isNotEmpty(search.getUserTel()),WmsCustomerInfo::getUserTel,search.getUserTel())
                        .orderByDesc(WmsCustomerInfo::getCreateDate)
        );
        ExcelUtil<WmsCustomerInfoExcel> util = new ExcelUtil<WmsCustomerInfoExcel>(WmsCustomerInfoExcel.class);
        util.exportExcel(response, wmsCustomerInfoMapStructMapper.infosToExcels(list), "客户数据");
    }


    /**
     * 获得客户字典数据
     * @return
     */
    @GetMapping("/getDictList")
    public AjaxResult getDictList(){
        List<WmsCustomerInfo> list = iWmsCustomerInfoService.list();
        return AjaxResult.success(wmsDictMapStructMapper.customersToDicts(list));
    }
}

