package com.ruoyi.wms.controller.form;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 仓库管理
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsWarehouseInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
              /**
     * 仓库编码
     */
    @NotEmpty(message = "仓库编码不能为空")
    private String warehouseCode;
        /**
     * 仓库名称
     */
    @NotEmpty(message = "仓库名称不能为空")
    private String warehouseName;
        /**
     * 仓库类型 字典表wms_warehouse_type
     */
    //@NotEmpty(message = "仓库类型 字典表wms_warehouse_type不能为空")
    private String warehouseType;
        /**
     * 状态 0：禁用 10：可用 20：锁定
     */
   // @NotEmpty(message = "状态 0：禁用 10：可用 20：锁定不能为空")
    private String warehouseStatus;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

    /**
     * 面积
     */
    private BigDecimal warehouseArea;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;
    /**
     * 地址
     */
    private String address;

  }
