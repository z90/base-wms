package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wms.controller.form.WmsProductTypeInfoForm;
import com.ruoyi.wms.controller.search.ProductTypeSearch;
import com.ruoyi.wms.entity.WmsWarehouseInfo;
import com.ruoyi.wms.entity.WmsWarehouseLocationInfo;
import com.ruoyi.wms.mapstruct.WmsDictMapStructMapper;
import com.ruoyi.wms.mapstruct.WmsProductTypeInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsProductTypeInfoService;



import com.ruoyi.wms.entity.WmsProductTypeInfo;
import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 商品分类 前端控制器实体类（WmsProductTypeInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsProductTypeInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsProductTypeInfoController extends BaseController {
    private final IWmsProductTypeInfoService iWmsProductTypeInfoService;
    private final WmsProductTypeInfoMapStructMapper wmsProductTypeInfoMapStructMapper;
    private final WmsDictMapStructMapper wmsDictMapStructMapper;

    /**
     * 新增商品分类
     * @param form
     * @return
     */
    @PostMapping("saveWmsProductTypeInfo")
    public AjaxResult saveWmsProductTypeInfo(@Valid @RequestBody WmsProductTypeInfoForm form){
        WmsProductTypeInfo wmsProductTypeInfo = wmsProductTypeInfoMapStructMapper.formToWmsProductTypeInfo(form);
        iWmsProductTypeInfoService.save(wmsProductTypeInfo);
        return AjaxResult.success();
    }
    /**
     * 商品分类删除
     * @param form
     * @return
     */
    @DeleteMapping("/delWmsProductTypeInfo/{id}")
    public AjaxResult delWmsProductTypeInfo(@PathVariable String id){
        iWmsProductTypeInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 商品分类修改
    * @return
    */
    @PutMapping("updWmsProductTypeInfo")
    public AjaxResult updWmsProductTypeInfo(@Valid @RequestBody WmsProductTypeInfoForm form){
        WmsProductTypeInfo wmsProductTypeInfo = wmsProductTypeInfoMapStructMapper.formToWmsProductTypeInfo(form);
        iWmsProductTypeInfoService.updateById(wmsProductTypeInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得商品分类列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsProductTypeInfoList")
    public TableDataInfo findWmsProductTypeInfoList(ProductTypeSearch search){
        startPage();
        List<WmsProductTypeInfo> list = iWmsProductTypeInfoService.list(
                Wrappers.<WmsProductTypeInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getTypeCode()),WmsProductTypeInfo::getTypeCode,search.getTypeCode())
                        .like(StrUtil.isNotEmpty(search.getTypeName()),WmsProductTypeInfo::getTypeName,search.getTypeName())
                        .orderByDesc(WmsProductTypeInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得商品分类详情通过ID
     * @return AjaxResult(WmsProductTypeInfo)
     */
    @RequestMapping("findWmsProductTypeInfoById/{id}")
    public AjaxResult findWmsProductTypeInfoById(@PathVariable String id){
        WmsProductTypeInfo wmsProductTypeInfo = iWmsProductTypeInfoService.getById(id);
        
        return AjaxResult.success(wmsProductTypeInfo);
    }

    /**
     * 获得产品类型 字典格式列表
     * @return 产品类型
     */
    @GetMapping("/getDictList")
    public AjaxResult getDictList(){
        List<WmsProductTypeInfo> list = iWmsProductTypeInfoService.list(
                Wrappers.<WmsProductTypeInfo>lambdaQuery().orderByAsc(WmsProductTypeInfo::getOrderBy).orderByDesc(WmsProductTypeInfo::getCreateDate)
        );
        return AjaxResult.success(wmsDictMapStructMapper.productTypesToDicts(list));
    }
}

