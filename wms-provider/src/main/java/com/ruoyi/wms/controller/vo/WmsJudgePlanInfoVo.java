package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 盘点计划信息
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsJudgePlanInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 盘点id
     */
    private String judgeId;
    /**
     * 盘点内容编码
     */
    private String judgeCode;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
}
