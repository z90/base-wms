package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsProductInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
    private String productCode;
        /**
     * 产品名称
     */
   // @NotEmpty(message = "产品名称不能为空")
    private String productName;
        /**
     * 产品类型
     */
   // @NotEmpty(message = "产品类型不能为空")
    private String productType;
        /**
     * 单位 字典 wms_product_unit
     */
   // @NotEmpty(message = "单位 字典 wms_product_unit不能为空")
    private String productUnit;
        /**
     * 来源 wms_product_source
     */
   // @NotEmpty(message = "来源 wms_product_source不能为空")
    private String source;
        /**
     * 最小库存
     */
   // @NotEmpty(message = "最小库存不能为空")
    private BigDecimal minStock;
        /**
     * 最大库存
     */
   // @NotEmpty(message = "最大库存不能为空")
    private BigDecimal maxStock;
        /**
     * 状态 Y: 启用 N:禁用
     */
   // @NotEmpty(message = "状态 Y: 启用 N:禁用不能为空")
    private String lockFlag;
        /**
     * 保质期
     */
   // @NotEmpty(message = "保质期不能为空")
    private Integer advent;
        /**
     * 状态 Y: 启用 N:禁用
     */
   // @NotEmpty(message = "状态 Y: 启用 N:禁用不能为空")
    private String status;
        /**
     * 状态 Y: 启用 N:禁用
     */
   // @NotEmpty(message = "状态 Y: 启用 N:禁用不能为空")
    private String isFifo;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

    /**
     * 规格
     */
    private String specUnit;

  }
