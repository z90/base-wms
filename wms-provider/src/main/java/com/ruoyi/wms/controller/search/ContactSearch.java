package com.ruoyi.wms.controller.search;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author zxg
 * @date 2023-11-09
 * @Desc
 */
@Data
public class ContactSearch {
    @NotEmpty(message = "数据不存在")
    private String sourceId;
}
