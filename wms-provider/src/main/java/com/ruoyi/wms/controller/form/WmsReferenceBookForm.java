package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 备查簿
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsReferenceBookForm implements Serializable {

  private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
              /**
     * 产品编码
     */
   // @NotEmpty(message = "产品编码不能为空")
    private String productCode;
        /**
     * 产品名称
     */
   // @NotEmpty(message = "产品名称不能为空")
    private String productName;
        /**
     * 产品类型
     */
   // @NotEmpty(message = "产品类型不能为空")
    private String productType;
        /**
     * 单位 字典 wms_product_unit
     */
   // @NotEmpty(message = "单位 字典 wms_product_unit不能为空")
    private String productUnit;
        /**
     * 更新数量
     */
   // @NotEmpty(message = "更新数量不能为空")
    private BigDecimal updateNum;
        /**
     * 更新前数量
     */
   // @NotEmpty(message = "更新前数量不能为空")
    private BigDecimal beforeNum;
        /**
     * 更新后数量
     */
   // @NotEmpty(message = "更新后数量不能为空")
    private BigDecimal afterNum;
        /**
     * 类型
     */
   // @NotEmpty(message = "类型不能为空")
    private String actionType;
        /**
     * 来源
     */
   // @NotEmpty(message = "来源不能为空")
    private String sourceCode;
        /**
     * 目标
     */
   // @NotEmpty(message = "目标不能为空")
    private String targetCode;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
