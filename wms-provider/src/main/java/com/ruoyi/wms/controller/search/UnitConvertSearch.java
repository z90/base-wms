package com.ruoyi.wms.controller.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Data
public class UnitConvertSearch {
    private String productCode;
}
