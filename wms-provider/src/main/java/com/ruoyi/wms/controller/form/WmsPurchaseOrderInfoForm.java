package com.ruoyi.wms.controller.form;
import java.math.BigDecimal;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 采购订单
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsPurchaseOrderInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;
    /**
     * 订单名称
     */
    @NotEmpty(message = "订单名称不能为空")
    @Max(value = 200,message = "订单名称不能多于{value}个字符")
    private String orderName;

        /**
     * 供应商编码
     */
    @NotEmpty(message = "供应商不能为空")
    private String supplierCode;

    /**
     * 联系人id
     */
    @NotEmpty(message = "联系人不能为空")
    private String contactId;

    /**
     * 订单明细
     */
    @Size(min = 1,message = "请至少选择一条商品")
    private List<OrderDetailForm> list;

  }
