package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 订单详情
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsOrderDetailInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 产品编码
     */
    private String productCode;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品类型
     */
    private String productType;
    /**
     * 单位 字典 wms_product_unit
     */
    private String productUnit;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 状态 0：进行中 10：完成 99：关闭
     */
    private Integer status;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
}
