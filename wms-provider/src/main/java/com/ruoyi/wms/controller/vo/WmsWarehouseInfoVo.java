package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 仓库管理
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsWarehouseInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 仓库编码
     */
    private String warehouseCode;
    /**
     * 仓库名称
     */
    private String warehouseName;
    /**
     * 仓库类型 字典表wms_warehouse_type
     */
    private String warehouseType;
    /**
     * 状态 0：禁用 10：可用 20：锁定
     */
    private String warehouseStatus;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;

    /**
     * 面积
     */
    private BigDecimal warehouseArea;

    /**
     * 联系人
     */
    private String userName;

    /**
     * 联系电话
     */
    private String userTel;
    /**
     * 地址
     */
    private String address;
}
