package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * <p>
 * 商品分类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsProductTypeInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 类型名称
     */
    private String typeName;
    /**
     * 类型编码
     */
    private String typeCode;
    /**
     * 上级id
     */
    private String parentId;
    /**
     * Y:启用 N:禁用
     */
    private String status;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
}
