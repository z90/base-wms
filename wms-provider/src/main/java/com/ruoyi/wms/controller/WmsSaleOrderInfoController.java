package com.ruoyi.wms.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wms.controller.form.WmsSaleOrderInfoForm;
import com.ruoyi.wms.controller.search.OrderSearch;
import com.ruoyi.wms.mapstruct.WmsSaleOrderInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsSaleOrderInfoService;
import com.ruoyi.wms.entity.WmsSaleOrderInfo;
import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 销售订单 前端控制器实体类（WmsSaleOrderInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsSaleOrderInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsSaleOrderInfoController extends BaseController {
    private final IWmsSaleOrderInfoService iWmsSaleOrderInfoService;
    private final WmsSaleOrderInfoMapStructMapper wmsSaleOrderInfoMapStructMapper;

    /**
     * 新增销售订单
     * @param form
     * @return
     */
    @PostMapping("saveWmsSaleOrderInfo")
    public AjaxResult saveWmsSaleOrderInfo(@Valid @RequestBody WmsSaleOrderInfoForm form){
        iWmsSaleOrderInfoService.insertOrderInfo(form);
        return AjaxResult.success();
    }

    /**
     * 取消订单
     * @param id 订单id
     * @return 取消结果
     */
    @GetMapping("/cancelOrder/{id}")
    public AjaxResult cancelOrder(@PathVariable String id){
        iWmsSaleOrderInfoService.cancelOrderById(id);
        return AjaxResult.success("取消成功");
    }

    /**
     * 重启成功
     * @param id 订单id
     * @return
     */
    @GetMapping("/restartOrder/{id}")
    public AjaxResult restartOrder(@PathVariable String id){
        iWmsSaleOrderInfoService.restartOrderById(id);
        return AjaxResult.success("重启成功");
    }

    /**
     * 销售订单删除
     * @param form
     * @return
     */
    @DeleteMapping("/delWmsSaleOrderInfo/{id}")
    public AjaxResult delWmsSaleOrderInfo(@PathVariable String id){
        iWmsSaleOrderInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 销售订单修改
    * @return
    */
    @PostMapping("updWmsSaleOrderInfo")
    public AjaxResult updWmsSaleOrderInfo(@Valid @RequestBody WmsSaleOrderInfoForm form){
        WmsSaleOrderInfo wmsSaleOrderInfo = wmsSaleOrderInfoMapStructMapper.formToWmsSaleOrderInfo(form);
        iWmsSaleOrderInfoService.updateById(wmsSaleOrderInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得销售订单列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsSaleOrderInfoList")
    public TableDataInfo findWmsSaleOrderInfoList(OrderSearch search){
        startPage();
        List<WmsSaleOrderInfo> list = iWmsSaleOrderInfoService.list(
                Wrappers.<WmsSaleOrderInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getOrderName()),WmsSaleOrderInfo::getOrderName,search.getOrderName())
                        .like(StrUtil.isNotEmpty(search.getOrderNo()),WmsSaleOrderInfo::getOrderNo,search.getOrderNo())
                        .like(StrUtil.isNotEmpty(search.getKeyword()),WmsSaleOrderInfo::getCustomerName,search.getKeyword())
                        .eq(search.getOrderStatus()!=null,WmsSaleOrderInfo::getStatus,search.getOrderStatus())
                        .orderByDesc(WmsSaleOrderInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得销售订单详情通过ID
     * @return AjaxResult(WmsSaleOrderInfo)
     */
    @RequestMapping("findWmsSaleOrderInfoById/{id}")
    public AjaxResult findWmsSaleOrderInfoById(@PathVariable String id){
        WmsSaleOrderInfo wmsSaleOrderInfo = iWmsSaleOrderInfoService.getById(id);
        
        return AjaxResult.success(wmsSaleOrderInfo);
    }

    /**
     * 通过订单号获得订单信息
     * @param orderNo 订单号
     * @return 订单信息
     */
    @RequestMapping("findWmsSaleOrderInfoByOrderNo/{orderNo}")
    public AjaxResult findWmsSaleOrderInfoByOrderNo(@PathVariable String orderNo){
        WmsSaleOrderInfo wmsSaleOrderInfo = iWmsSaleOrderInfoService.getByOrderNo(orderNo);

        return AjaxResult.success(wmsSaleOrderInfo);
    }
}

