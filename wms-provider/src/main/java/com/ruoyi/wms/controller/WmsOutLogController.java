package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsOutLogForm;
import com.ruoyi.wms.mapstruct.WmsOutLogMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsOutLogService;



import com.ruoyi.wms.entity.WmsOutLog;
import javax.validation.Valid;




/**
 * <p>
 * 出库记录 前端控制器实体类（WmsOutLog）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsOutLog")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsOutLogController extends BaseController {
    private final IWmsOutLogService iWmsOutLogService;
    private final WmsOutLogMapStructMapper wmsOutLogMapStructMapper;

    /**
     * 新增出库记录
     * @param form
     * @return
     */
    @PostMapping("saveWmsOutLog")
    public AjaxResult saveWmsOutLog(@Valid @RequestBody WmsOutLogForm form){
        WmsOutLog wmsOutLog = wmsOutLogMapStructMapper.formToWmsOutLog(form);
        iWmsOutLogService.save(wmsOutLog);
        return AjaxResult.success();
    }
    /**
     * 出库记录删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsOutLog(@PathVariable String id){
        iWmsOutLogService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 出库记录修改
    * @return
    */
    @PostMapping("updWmsOutLog")
    public AjaxResult updWmsOutLog(@Valid @RequestBody WmsOutLogForm form){
        WmsOutLog wmsOutLog = wmsOutLogMapStructMapper.formToWmsOutLog(form);
        iWmsOutLogService.updateById(wmsOutLog);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得出库记录列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsOutLogList")
    public AjaxResult findWmsOutLogList(Page<WmsOutLog> page){
        iWmsOutLogService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得出库记录详情通过ID
     * @return AjaxResult(WmsOutLog)
     */
    @RequestMapping("findWmsOutLogById")
    public AjaxResult findWmsOutLogById(@PathVariable String id){
        WmsOutLog wmsOutLog = iWmsOutLogService.getById(id);
        
        return AjaxResult.success(wmsOutLog);
    }
}

