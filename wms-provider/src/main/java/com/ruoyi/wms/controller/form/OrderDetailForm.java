package com.ruoyi.wms.controller.form;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * @author zxg
 * @date 2023-11-10
 * @Desc
 */
@Data
public class OrderDetailForm {

    /**
     * 产品编码
     */
    // @NotEmpty(message = "产品编码不能为空")
    private String productCode;

    /**
     * 数量
     */
    @DecimalMin(value = "1",message = "产品数量不能少于{1}")
    private BigDecimal num;

    /**
     * 订单金额
     */
    private BigDecimal orderPrice;
}
