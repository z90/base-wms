package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsReferenceBookForm;
import com.ruoyi.wms.mapstruct.WmsReferenceBookMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsReferenceBookService;



import com.ruoyi.wms.entity.WmsReferenceBook;
import javax.validation.Valid;




/**
 * <p>
 * 备查簿 前端控制器实体类（WmsReferenceBook）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsReferenceBook")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsReferenceBookController extends BaseController {
    private final IWmsReferenceBookService iWmsReferenceBookService;
    private final WmsReferenceBookMapStructMapper wmsReferenceBookMapStructMapper;

    /**
     * 新增备查簿
     * @param form
     * @return
     */
    @PostMapping("saveWmsReferenceBook")
    public AjaxResult saveWmsReferenceBook(@Valid @RequestBody WmsReferenceBookForm form){
        WmsReferenceBook wmsReferenceBook = wmsReferenceBookMapStructMapper.formToWmsReferenceBook(form);
        iWmsReferenceBookService.save(wmsReferenceBook);
        return AjaxResult.success();
    }
    /**
     * 备查簿删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsReferenceBook(@PathVariable String id){
        iWmsReferenceBookService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 备查簿修改
    * @return
    */
    @PostMapping("updWmsReferenceBook")
    public AjaxResult updWmsReferenceBook(@Valid @RequestBody WmsReferenceBookForm form){
        WmsReferenceBook wmsReferenceBook = wmsReferenceBookMapStructMapper.formToWmsReferenceBook(form);
        iWmsReferenceBookService.updateById(wmsReferenceBook);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得备查簿列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsReferenceBookList")
    public AjaxResult findWmsReferenceBookList(Page<WmsReferenceBook> page){
        iWmsReferenceBookService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得备查簿详情通过ID
     * @return AjaxResult(WmsReferenceBook)
     */
    @RequestMapping("findWmsReferenceBookById")
    public AjaxResult findWmsReferenceBookById(@PathVariable String id){
        WmsReferenceBook wmsReferenceBook = iWmsReferenceBookService.getById(id);
        
        return AjaxResult.success(wmsReferenceBook);
    }
}

