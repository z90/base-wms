package com.ruoyi.wms.controller.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-11-07
 * @Desc
 */
@Data
public class ProductSearch {

    /**
     * 产品编码
     */
    private String productCode;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品类型
     */
    private String productType;
}
