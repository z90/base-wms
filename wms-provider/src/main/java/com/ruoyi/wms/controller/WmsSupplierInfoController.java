package com.ruoyi.wms.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wms.controller.excel.WmsSupplierInfoExcel;
import com.ruoyi.wms.controller.form.WmsSupplierInfoForm;
import com.ruoyi.wms.controller.search.SupplierSearch;
import com.ruoyi.wms.mapstruct.WmsDictMapStructMapper;
import com.ruoyi.wms.mapstruct.WmsSupplierInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsSupplierInfoService;



import com.ruoyi.wms.entity.WmsSupplierInfo;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 供应商 前端控制器实体类（WmsSupplierInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsSupplierInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsSupplierInfoController extends BaseController {
    private final IWmsSupplierInfoService iWmsSupplierInfoService;
    private final WmsSupplierInfoMapStructMapper wmsSupplierInfoMapStructMapper;
    private final WmsDictMapStructMapper wmsDictMapStructMapper;

    /**
     * 新增供应商
     * @param form
     * @return
     */
    @PostMapping("saveWmsSupplierInfo")
    public AjaxResult saveWmsSupplierInfo(@Valid @RequestBody WmsSupplierInfoForm form){
        WmsSupplierInfo wmsSupplierInfo = wmsSupplierInfoMapStructMapper.formToWmsSupplierInfo(form);
        WmsSupplierInfo info = iWmsSupplierInfoService.findByCode(form.getSupplierCode());
        if(!BeanUtil.isEmpty(info)){
            return AjaxResult.error("供应商编码已存在");
        }
        iWmsSupplierInfoService.save(wmsSupplierInfo);
        return AjaxResult.success();
    }
    /**
     * 供应商删除
     * @param form
     * @return
     */
    @DeleteMapping("delWmsSupplierInfo/{id}")
    public AjaxResult delWmsSupplierInfo(@PathVariable String id){
        iWmsSupplierInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 供应商修改
    * @return
    */
    @PutMapping("updWmsSupplierInfo")
    public AjaxResult updWmsSupplierInfo(@Valid @RequestBody WmsSupplierInfoForm form){
        WmsSupplierInfo wmsSupplierInfo = wmsSupplierInfoMapStructMapper.formToWmsSupplierInfo(form);
        WmsSupplierInfo info = iWmsSupplierInfoService.findByCode(form.getSupplierCode());
        if(!BeanUtil.isEmpty(info) && !info.getId().equals(form.getSupplierCode())){
            return AjaxResult.error("供应商编码已存在");
        }
        iWmsSupplierInfoService.updateById(wmsSupplierInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得供应商列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsSupplierInfoList")
    public TableDataInfo findWmsSupplierInfoList(SupplierSearch search){
        startPage();
        List<WmsSupplierInfo> list = iWmsSupplierInfoService.list(
                Wrappers.<WmsSupplierInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getUserTel()),WmsSupplierInfo::getUserTel,search.getUserTel())
                        .like(StrUtil.isNotEmpty(search.getSupplierType()),WmsSupplierInfo::getSupplierType,search.getSupplierType())
                        .like(StrUtil.isNotEmpty(search.getSupplierCode()),WmsSupplierInfo::getSupplierCode,search.getSupplierCode())
                        .like(StrUtil.isNotEmpty(search.getSupplierName()),WmsSupplierInfo::getSupplierName,search.getSupplierName())
                        .orderByDesc(WmsSupplierInfo::getCreateDate)
        );
        return getDataTable(list);
    }

    /**
     * 获得供应商详情通过ID
     * @return AjaxResult(WmsSupplierInfo)
     */
    @RequestMapping("findWmsSupplierInfoById/{id}")
    public AjaxResult findWmsSupplierInfoById(@PathVariable String id){
        WmsSupplierInfo wmsSupplierInfo = iWmsSupplierInfoService.getById(id);
        
        return AjaxResult.success(wmsSupplierInfo);
    }

    /**
     * 供应商数据导出
     * @param response
     * @param search
     */
    @Log(title = "供应商管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SupplierSearch search)
    {
        List<WmsSupplierInfo> list = iWmsSupplierInfoService.list(
                Wrappers.<WmsSupplierInfo>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getUserTel()),WmsSupplierInfo::getUserTel,search.getUserTel())
                        .like(StrUtil.isNotEmpty(search.getSupplierType()),WmsSupplierInfo::getSupplierType,search.getSupplierType())
                        .like(StrUtil.isNotEmpty(search.getSupplierCode()),WmsSupplierInfo::getSupplierCode,search.getSupplierCode())
                        .like(StrUtil.isNotEmpty(search.getSupplierName()),WmsSupplierInfo::getSupplierName,search.getSupplierName())
                        .orderByDesc(WmsSupplierInfo::getCreateDate)
        );
        ExcelUtil<WmsSupplierInfoExcel> util = new ExcelUtil<WmsSupplierInfoExcel>(WmsSupplierInfoExcel.class);
        util.exportExcel(response, wmsSupplierInfoMapStructMapper.infosToExcels(list), "供应商数据");
    }

    /**
     * 获得供应商列表
     * @return
     */
    @GetMapping("/getDictList")
    public AjaxResult getDictList(){
        List<WmsSupplierInfo> list = iWmsSupplierInfoService.list();
        return AjaxResult.success(wmsDictMapStructMapper.suppliersToDicts(list));
    }
}

