package com.ruoyi.wms.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wms.controller.form.WmsProductUnitConvertForm;
import com.ruoyi.wms.controller.search.UnitConvertSearch;
import com.ruoyi.wms.entity.WmsProductInfo;
import com.ruoyi.wms.entity.WmsProductUnitConvert;
import com.ruoyi.wms.mapstruct.WmsProductUnitConvertStructMapper;
import com.ruoyi.wms.service.IWmsProductInfoService;
import com.ruoyi.wms.service.IWmsProductUnitConvertService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Slf4j
@Validated
@RestController
@AllArgsConstructor
@RequestMapping("/wmsProductUnitConvert")
public class WmsProductUnitConvertController extends BaseController {

    private final IWmsProductUnitConvertService iWmsProductUnitConvertService;
    private final IWmsProductInfoService iWmsProductInfoService;

    private final WmsProductUnitConvertStructMapper wmsProductUnitConvertStructMapper;

    /**
     * 新增单位转换
     * @param form
     * @return
     */
    @PostMapping("saveWmsProductUnitConvert")
    public AjaxResult saveWmsProductUnitConvert(@Valid @RequestBody WmsProductUnitConvertForm form){
        WmsProductUnitConvert wmsProductUnitConvert = wmsProductUnitConvertStructMapper.fromToInfo(form);
        WmsProductInfo info = iWmsProductInfoService.getByCode(form.getProductCode());
        if(BeanUtil.isEmpty(info)){
            return AjaxResult.error("商品不存在");
        }
        iWmsProductUnitConvertService.save(wmsProductUnitConvert);
        return AjaxResult.success();
    }
    /**
     * 单位转换信息删除
     * @return
     */
    @DeleteMapping("/delWmsProductUnitConvert/{id}")
    public AjaxResult delWmsProductUnitConvert(@PathVariable String id){
        iWmsProductUnitConvertService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
     * 单位转换信息修改
     * @return
     */
    @PutMapping("updWmsProductUnitConvert")
    public AjaxResult updWmsProductUnitConvert(@Valid @RequestBody WmsProductUnitConvertForm form){
        WmsProductUnitConvert wmsProductUnitConvert = wmsProductUnitConvertStructMapper.fromToInfo(form);
        WmsProductInfo info = iWmsProductInfoService.getByCode(form.getProductCode());
        if(BeanUtil.isEmpty(info)){
            return AjaxResult.error("商品不存在");
        }
        iWmsProductUnitConvertService.updateById(wmsProductUnitConvert);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得单位转换列表
     * @return
     */
    @RequestMapping("findWmsProductUnitConvertList")
    public TableDataInfo findWmsProductUnitConvertList(UnitConvertSearch search){
        startPage();
        List<WmsProductUnitConvert> list = iWmsProductUnitConvertService.list(
                Wrappers.<WmsProductUnitConvert>lambdaQuery()
                        .like(StrUtil.isNotEmpty(search.getProductCode()),WmsProductUnitConvert::getProductCode,search.getProductCode())
                        .orderByDesc(WmsProductUnitConvert::getCreateDate)
        );
        list.forEach(item->{
            WmsProductInfo wmsProductInfo = iWmsProductInfoService.getByCode(item.getProductCode());
            if(wmsProductInfo!=null){
                item.setProductName(wmsProductInfo.getProductName());
            }
        });
        return getDataTable(list);
    }

    /**
     * 获得单位转换详情通过ID
     * @return Result(WmsCustomerInfo)
     */
    @RequestMapping("findWmsProductUnitConvertById/{id}")
    public AjaxResult findWmsCustomerInfoById(@PathVariable String id){
        WmsProductUnitConvert wmsProductUnitConvert = iWmsProductUnitConvertService.getById(id);
        return AjaxResult.success(wmsProductUnitConvert);
    }
}
