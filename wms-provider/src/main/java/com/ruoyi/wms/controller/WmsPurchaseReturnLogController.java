package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsPurchaseReturnLogForm;
import com.ruoyi.wms.mapstruct.WmsPurchaseReturnLogMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsPurchaseReturnLogService;



import com.ruoyi.wms.entity.WmsPurchaseReturnLog;
import javax.validation.Valid;




/**
 * <p>
 * 销售退货 前端控制器实体类（WmsPurchaseReturnLog）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsPurchaseReturnLog")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsPurchaseReturnLogController extends BaseController {
    private final IWmsPurchaseReturnLogService iWmsPurchaseReturnLogService;
    private final WmsPurchaseReturnLogMapStructMapper wmsPurchaseReturnLogMapStructMapper;

    /**
     * 新增销售退货
     * @param form
     * @return
     */
    @PostMapping("saveWmsPurchaseReturnLog")
    public AjaxResult saveWmsPurchaseReturnLog(@Valid @RequestBody WmsPurchaseReturnLogForm form){
        WmsPurchaseReturnLog wmsPurchaseReturnLog = wmsPurchaseReturnLogMapStructMapper.formToWmsPurchaseReturnLog(form);
        iWmsPurchaseReturnLogService.save(wmsPurchaseReturnLog);
        return AjaxResult.success();
    }
    /**
     * 销售退货删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsPurchaseReturnLog(@PathVariable String id){
        iWmsPurchaseReturnLogService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 销售退货修改
    * @return
    */
    @PostMapping("updWmsPurchaseReturnLog")
    public AjaxResult updWmsPurchaseReturnLog(@Valid @RequestBody WmsPurchaseReturnLogForm form){
        WmsPurchaseReturnLog wmsPurchaseReturnLog = wmsPurchaseReturnLogMapStructMapper.formToWmsPurchaseReturnLog(form);
        iWmsPurchaseReturnLogService.updateById(wmsPurchaseReturnLog);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得销售退货列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsPurchaseReturnLogList")
    public AjaxResult findWmsPurchaseReturnLogList(Page<WmsPurchaseReturnLog> page){
        iWmsPurchaseReturnLogService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得销售退货详情通过ID
     * @return AjaxResult(WmsPurchaseReturnLog)
     */
    @RequestMapping("findWmsPurchaseReturnLogById")
    public AjaxResult findWmsPurchaseReturnLogById(@PathVariable String id){
        WmsPurchaseReturnLog wmsPurchaseReturnLog = iWmsPurchaseReturnLogService.getById(id);
        
        return AjaxResult.success(wmsPurchaseReturnLog);
    }
}

