package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsSaleReturnLogForm;
import com.ruoyi.wms.mapstruct.WmsSaleReturnLogMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsSaleReturnLogService;



import com.ruoyi.wms.entity.WmsSaleReturnLog;
import javax.validation.Valid;




/**
 * <p>
 * 销售退货 前端控制器实体类（WmsSaleReturnLog）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsSaleReturnLog")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsSaleReturnLogController extends BaseController {
    private final IWmsSaleReturnLogService iWmsSaleReturnLogService;
    private final WmsSaleReturnLogMapStructMapper wmsSaleReturnLogMapStructMapper;

    /**
     * 新增销售退货
     * @param form
     * @return
     */
    @PostMapping("saveWmsSaleReturnLog")
    public AjaxResult saveWmsSaleReturnLog(@Valid @RequestBody WmsSaleReturnLogForm form){
        WmsSaleReturnLog wmsSaleReturnLog = wmsSaleReturnLogMapStructMapper.formToWmsSaleReturnLog(form);
        iWmsSaleReturnLogService.save(wmsSaleReturnLog);
        return AjaxResult.success();
    }
    /**
     * 销售退货删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsSaleReturnLog(@PathVariable String id){
        iWmsSaleReturnLogService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 销售退货修改
    * @return
    */
    @PostMapping("updWmsSaleReturnLog")
    public AjaxResult updWmsSaleReturnLog(@Valid @RequestBody WmsSaleReturnLogForm form){
        WmsSaleReturnLog wmsSaleReturnLog = wmsSaleReturnLogMapStructMapper.formToWmsSaleReturnLog(form);
        iWmsSaleReturnLogService.updateById(wmsSaleReturnLog);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得销售退货列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsSaleReturnLogList")
    public AjaxResult findWmsSaleReturnLogList(Page<WmsSaleReturnLog> page){
        iWmsSaleReturnLogService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得销售退货详情通过ID
     * @return AjaxResult(WmsSaleReturnLog)
     */
    @RequestMapping("findWmsSaleReturnLogById")
    public AjaxResult findWmsSaleReturnLogById(@PathVariable String id){
        WmsSaleReturnLog wmsSaleReturnLog = iWmsSaleReturnLogService.getById(id);
        
        return AjaxResult.success(wmsSaleReturnLog);
    }
}

