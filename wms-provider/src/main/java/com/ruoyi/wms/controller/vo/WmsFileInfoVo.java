package com.ruoyi.wms.controller.vo;

import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * <p>
 * 
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsFileInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private String id;
    /**
     * 文件路径
     */
    private String filePath;
    /**
     * 文件网络路径
     */
    private String fileUrl;
    /**
     * 关联业务编码
     */
    private String businessCode;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateDate;
}
