package com.ruoyi.wms.controller.form;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 商品分类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsProductTypeInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 类型名称
     */
   // @NotEmpty(message = "类型名称不能为空")
    private String typeName;
        /**
     * 类型编码
     */
   // @NotEmpty(message = "类型编码不能为空")
    private String typeCode;
        /**
     * 上级id
     */
   // @NotEmpty(message = "上级id不能为空")
    private String parentId;
        /**
     * Y:启用 N:禁用
     */
   // @NotEmpty(message = "Y:启用 N:禁用不能为空")
    private String status;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
