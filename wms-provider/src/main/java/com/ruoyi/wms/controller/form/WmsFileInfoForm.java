package com.ruoyi.wms.controller.form;
import java.time.LocalDateTime;
import lombok.Data;
import java.io.Serializable;
/**
 * <p>
 * 
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsFileInfoForm implements Serializable {

  private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
              /**
     * 文件路径
     */
   // @NotEmpty(message = "文件路径不能为空")
    private String filePath;
        /**
     * 文件网络路径
     */
   // @NotEmpty(message = "文件网络路径不能为空")
    private String fileUrl;
        /**
     * 关联业务编码
     */
   // @NotEmpty(message = "关联业务编码不能为空")
    private String businessCode;
        /**
     * 排序
     */
   // @NotEmpty(message = "排序不能为空")
    private Integer orderBy;
        /**
     * 备注
     */
   // @NotEmpty(message = "备注不能为空")
    private String remark;

  }
