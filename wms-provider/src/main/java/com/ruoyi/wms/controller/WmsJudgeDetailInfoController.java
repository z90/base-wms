package com.ruoyi.wms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wms.controller.form.WmsJudgeDetailInfoForm;
import com.ruoyi.wms.mapstruct.WmsJudgeDetailInfoMapStructMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.wms.service.IWmsJudgeDetailInfoService;
import com.ruoyi.wms.entity.WmsJudgeDetailInfo;
import javax.validation.Valid;


/**
 * <p>
 * 判断详情 前端控制器实体类（WmsJudgeDetailInfo）
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@RestController
@RequestMapping("wmsJudgeDetailInfo")
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class WmsJudgeDetailInfoController extends BaseController {
    private final IWmsJudgeDetailInfoService iWmsJudgeDetailInfoService;
    private final WmsJudgeDetailInfoMapStructMapper wmsJudgeDetailInfoMapStructMapper;

    /**
     * 新增判断详情
     * @param form
     * @return
     */
    @PostMapping("saveWmsJudgeDetailInfo")
    public AjaxResult saveWmsJudgeDetailInfo(@Valid @RequestBody WmsJudgeDetailInfoForm form){
        WmsJudgeDetailInfo wmsJudgeDetailInfo = wmsJudgeDetailInfoMapStructMapper.formToWmsJudgeDetailInfo(form);
        iWmsJudgeDetailInfoService.save(wmsJudgeDetailInfo);
        return AjaxResult.success();
    }
    /**
     * 判断详情删除
     * @param form
     * @return
     */
    @DeleteMapping("{id}")
    public AjaxResult delWmsJudgeDetailInfo(@PathVariable String id){
        iWmsJudgeDetailInfoService.removeById(id);
        return AjaxResult.success("删除成功");
    }
    /**
    * 判断详情修改
    * @return
    */
    @PostMapping("updWmsJudgeDetailInfo")
    public AjaxResult updWmsJudgeDetailInfo(@Valid @RequestBody WmsJudgeDetailInfoForm form){
        WmsJudgeDetailInfo wmsJudgeDetailInfo = wmsJudgeDetailInfoMapStructMapper.formToWmsJudgeDetailInfo(form);
        iWmsJudgeDetailInfoService.updateById(wmsJudgeDetailInfo);
        return AjaxResult.success("修改成功");
    }

    /**
     * 获得判断详情列表
     * @param page
     * @return
     */
    @RequestMapping("findWmsJudgeDetailInfoList")
    public AjaxResult findWmsJudgeDetailInfoList(Page<WmsJudgeDetailInfo> page){
        iWmsJudgeDetailInfoService.page(page);
        return AjaxResult.success(page);
    }

    /**
     * 获得判断详情详情通过ID
     * @return AjaxResult(WmsJudgeDetailInfo)
     */
    @RequestMapping("findWmsJudgeDetailInfoById")
    public AjaxResult findWmsJudgeDetailInfoById(@PathVariable String id){
        WmsJudgeDetailInfo wmsJudgeDetailInfo = iWmsJudgeDetailInfoService.getById(id);
        return AjaxResult.success(wmsJudgeDetailInfo);
    }
}

