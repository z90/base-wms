package com.ruoyi.wms.enums;

/**
 * @author zxg
 * @date 2023-11-10
 * @Desc 订单状态
 */
public enum OrderStatusEnums {
    /**
     * 进行中/未收货/未发货
     */
    ORDER_NEW(0,"进行中/未收货/未发货"),
    /**
     * 部分收货/发货
     */
    ORDER_PART(10,"部分收货/发货"),
    /**
     * 全部收货/发货
     */
    ORDER_ALL(20,"全部收货/发货"),
    /**
     * 取消
     */
    ORDER_CANCEL(99,"取消");

    public final int code;
    private String msg;

    OrderStatusEnums(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
