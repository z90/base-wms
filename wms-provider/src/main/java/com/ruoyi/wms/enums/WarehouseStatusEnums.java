package com.ruoyi.wms.enums;

/**
 * @author zxg
 * @date 2023-10-26
 * @Desc 仓库状态
 */
public enum WarehouseStatusEnums {
    /**
     * 禁用
     */
    WAREHOUSE_STATUS_0(0,"禁用"),
    /**
     * 启用
     */
    WAREHOUSE_STATUS_10(10,"启用"),
    /**
     * 锁定
     */
    WAREHOUSE_STATUS_20(20,"锁定");

    private Integer code;
    private String msg;

    WarehouseStatusEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
