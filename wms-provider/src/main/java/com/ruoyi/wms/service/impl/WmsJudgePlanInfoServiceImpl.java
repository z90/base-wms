package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsJudgePlanInfo;
import com.ruoyi.wms.mapper.WmsJudgePlanInfoMapper;
import com.ruoyi.wms.service.IWmsJudgePlanInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 盘点计划信息 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsJudgePlanInfoServiceImpl extends ServiceImpl<WmsJudgePlanInfoMapper, WmsJudgePlanInfo> implements IWmsJudgePlanInfoService {

}
