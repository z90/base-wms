package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsOutLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 出库记录 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsOutLogService extends IService<WmsOutLog> {

    /**
     * 记录出库日志
     * @param outLog 出库信息
     */
    void saveInfo(WmsOutLog outLog);
}
