package com.ruoyi.wms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.service.SequenceService;
import com.ruoyi.common.exception.BizBusinessException;
import com.ruoyi.wms.controller.form.OrderDetailForm;
import com.ruoyi.wms.controller.form.WmsPurchaseOrderInfoForm;
import com.ruoyi.wms.entity.*;
import com.ruoyi.wms.enums.OrderStatusEnums;
import com.ruoyi.wms.mapper.WmsPurchaseOrderInfoMapper;
import com.ruoyi.wms.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 采购订单 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@Service
public class WmsPurchaseOrderInfoServiceImpl extends ServiceImpl<WmsPurchaseOrderInfoMapper, WmsPurchaseOrderInfo> implements IWmsPurchaseOrderInfoService {

    @Resource(name = "cgOrderSequenceService")
    private SequenceService sequenceService;
    @Resource
    private IWmsProductInfoService iWmsProductInfoService;
    @Resource
    private IWmsContactInfoService iWmsContactInfoService;
    @Resource
    private IWmsSupplierInfoService iWmsSupplierInfoService;
    @Resource
    private IWmsOrderDetailInfoService iWmsOrderDetailInfoService;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertOrderInfo(WmsPurchaseOrderInfoForm form) {
        WmsPurchaseOrderInfo orderInfo = new WmsPurchaseOrderInfo();
        WmsSupplierInfo wmsSupplierInfo = iWmsSupplierInfoService.findByCode(form.getSupplierCode());
        if(BeanUtil.isEmpty(wmsSupplierInfo)){
            throw new BizBusinessException("供应商不存在");
        }
        WmsContactInfo wmsContactInfo = iWmsContactInfoService.getById(form.getContactId());
        if(BeanUtil.isEmpty(wmsContactInfo)){
            throw new BizBusinessException("联系人不存在");
        }
        String orderNo = sequenceService.generateSerialNumber();
        List<WmsOrderDetailInfo> detailInfoList = new ArrayList<>();
        BigDecimal totalAmount = BigDecimal.ZERO;
        for(OrderDetailForm item: form.getList()){
            WmsProductInfo wmsProductInfo = iWmsProductInfoService.getByCode(item.getProductCode());
            if(BeanUtil.isEmpty(wmsProductInfo)){
                throw new BizBusinessException("采购商品【"+item.getProductCode()+"】不存在");
            }
            WmsOrderDetailInfo detailInfo = new WmsOrderDetailInfo();
            BeanUtil.copyProperties(item,detailInfo);
            detailInfo.setOrderNo(orderNo);
            detailInfo.setActualPrice(item.getOrderPrice());
            if(item.getOrderPrice()!=null){
                totalAmount = totalAmount.add((detailInfo.getOrderPrice().multiply(detailInfo.getNum())));
            }
            detailInfoList.add(detailInfo);
        }
        orderInfo.setStatus(OrderStatusEnums.ORDER_NEW.getCode());
        orderInfo.setOrderNo(orderNo);
        orderInfo.setOrderName(form.getOrderName());
        orderInfo.setSupplierCode(wmsSupplierInfo.getSupplierCode());
        orderInfo.setSupplierName(wmsSupplierInfo.getSupplierName());
        orderInfo.setUserName(wmsContactInfo.getUserName());
        orderInfo.setUserTel(wmsContactInfo.getUserTel());
        orderInfo.setOrderAmount(totalAmount);
        orderInfo.setOrderNetAmount(totalAmount);
        save(orderInfo);
        iWmsOrderDetailInfoService.saveBatch(detailInfoList);

    }

    @Override
    public void cancelOrderById(String id) {
        update(
                Wrappers.<WmsPurchaseOrderInfo>lambdaUpdate().set(WmsPurchaseOrderInfo::getStatus,OrderStatusEnums.ORDER_CANCEL.getCode())
                        .eq(WmsPurchaseOrderInfo::getId,id)
        );
    }

    @Override
    public void restartOrderById(String id) {
        update(
                Wrappers.<WmsPurchaseOrderInfo>lambdaUpdate().set(WmsPurchaseOrderInfo::getStatus,OrderStatusEnums.ORDER_NEW.getCode())
                        .eq(WmsPurchaseOrderInfo::getId,id)
        );
    }

    @Override
    public WmsPurchaseOrderInfo getByOrderNo(String orderNo) {
        return getOne(Wrappers.<WmsPurchaseOrderInfo>lambdaQuery().eq(WmsPurchaseOrderInfo::getOrderNo,orderNo).last("limit 1"));

    }
}
