package com.ruoyi.wms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.wms.entity.WmsInLog;
import com.ruoyi.wms.entity.WmsReferenceBook;
import com.ruoyi.wms.mapper.WmsInLogMapper;
import com.ruoyi.wms.mapper.WmsLiveStockInfoMapper;
import com.ruoyi.wms.mapstruct.WmsReferenceBookMapStructMapper;
import com.ruoyi.wms.service.IWmsInLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.wms.service.IWmsReferenceBookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 入库记录 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
@AllArgsConstructor
public class WmsInLogServiceImpl extends ServiceImpl<WmsInLogMapper, WmsInLog> implements IWmsInLogService {

    private final IWmsReferenceBookService iWmsReferenceBookService;
    private final WmsLiveStockInfoMapper wmsLiveStockInfoMapper;
    private final WmsReferenceBookMapStructMapper wmsReferenceBookMapStructMapper;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveInfo(WmsInLog wmsInLog) {
        BigDecimal num = wmsLiveStockInfoMapper.getLiveCount(wmsInLog.getProductCode());
        WmsReferenceBook book = wmsReferenceBookMapStructMapper.inLogToWmsReferenceBook(wmsInLog);
        book.setBeforeNum(num);
        book.setUpdateNum(wmsInLog.getNum());
        book.setAfterNum(num.add(wmsInLog.getNum()));
        // 记录备查薄
        iWmsReferenceBookService.save(book);
    }
}
