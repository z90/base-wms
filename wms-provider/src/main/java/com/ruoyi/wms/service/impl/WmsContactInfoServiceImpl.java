package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsContactInfo;
import com.ruoyi.wms.mapper.WmsContactInfoMapper;
import com.ruoyi.wms.service.IWmsContactInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 供应商联系人 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsContactInfoServiceImpl extends ServiceImpl<WmsContactInfoMapper, WmsContactInfo> implements IWmsContactInfoService {

}
