package com.ruoyi.wms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.wms.entity.WmsCustomerInfo;
import com.ruoyi.wms.mapper.WmsCustomerInfoMapper;
import com.ruoyi.wms.service.IWmsCustomerInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户信息 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsCustomerInfoServiceImpl extends ServiceImpl<WmsCustomerInfoMapper, WmsCustomerInfo> implements IWmsCustomerInfoService {

    @Override
    public WmsCustomerInfo findByCode(String customerCode) {
        return getOne(Wrappers.<WmsCustomerInfo>lambdaQuery().eq(WmsCustomerInfo::getCustomerCode,customerCode).last("limit 1"));
    }
}
