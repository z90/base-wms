package com.ruoyi.wms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.service.SequenceService;
import com.ruoyi.common.exception.BizBusinessException;
import com.ruoyi.wms.controller.form.OrderDetailForm;
import com.ruoyi.wms.controller.form.WmsSaleOrderInfoForm;
import com.ruoyi.wms.entity.*;
import com.ruoyi.wms.enums.OrderStatusEnums;
import com.ruoyi.wms.mapper.WmsSaleOrderInfoMapper;
import com.ruoyi.wms.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 销售订单 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@Service
public class WmsSaleOrderInfoServiceImpl extends ServiceImpl<WmsSaleOrderInfoMapper, WmsSaleOrderInfo> implements IWmsSaleOrderInfoService {

    @Resource(name = "xsOrderSequenceService")
    private SequenceService sequenceService;
    @Resource
    private IWmsProductInfoService iWmsProductInfoService;
    @Resource
    private IWmsContactInfoService iWmsContactInfoService;
    @Resource
    private IWmsCustomerInfoService iWmsCustomerInfoService;
    @Resource
    private IWmsOrderDetailInfoService iWmsOrderDetailInfoService;
    @Override
    public void insertOrderInfo(WmsSaleOrderInfoForm form) {
        WmsSaleOrderInfo orderInfo = new WmsSaleOrderInfo();
        WmsCustomerInfo wmsCustomerInfo = iWmsCustomerInfoService.findByCode(form.getCustomerCode());
        if(BeanUtil.isEmpty(wmsCustomerInfo)){
            log.error("客户【"+form.getCustomerCode()+"】不存在");
            throw new BizBusinessException("客户不存在");
        }
        WmsContactInfo wmsContactInfo = iWmsContactInfoService.getById(form.getContactId());
        if(BeanUtil.isEmpty(wmsContactInfo)){
            throw new BizBusinessException("联系人不存在");
        }
        List<WmsOrderDetailInfo> detailInfoList = new ArrayList<>();
        String orderNo = sequenceService.generateSerialNumber();
        BigDecimal totalAmount = BigDecimal.ZERO;
        for(OrderDetailForm item: form.getList()){
            WmsProductInfo wmsProductInfo = iWmsProductInfoService.getByCode(item.getProductCode());
            if(BeanUtil.isEmpty(wmsProductInfo)){
                throw new BizBusinessException("销售商品【"+item.getProductCode()+"】不存在");
            }
            WmsOrderDetailInfo detailInfo = new WmsOrderDetailInfo();
            BeanUtil.copyProperties(item,detailInfo);
            detailInfo.setActualPrice(item.getOrderPrice());
            detailInfo.setOrderNo(orderNo);
            if(item.getOrderPrice()!=null){
                totalAmount = totalAmount.add((detailInfo.getOrderPrice().multiply(detailInfo.getNum())));
            }
            detailInfoList.add(detailInfo);
        }
        orderInfo.setStatus(OrderStatusEnums.ORDER_NEW.getCode());
        orderInfo.setOrderNo(orderNo);
        orderInfo.setOrderName(form.getOrderName());
        orderInfo.setCustomerName(wmsCustomerInfo.getCustomerName());
        orderInfo.setCustomerCode(wmsCustomerInfo.getCustomerCode());
        orderInfo.setUserName(wmsContactInfo.getUserName());
        orderInfo.setUserTel(wmsContactInfo.getUserTel());
        orderInfo.setOrderAmount(totalAmount);
        orderInfo.setOrderNetAmount(totalAmount);
        save(orderInfo);
        iWmsOrderDetailInfoService.saveBatch(detailInfoList);
    }

    @Override
    public void cancelOrderById(String id) {
        update(
                Wrappers.<WmsSaleOrderInfo>lambdaUpdate().set(WmsSaleOrderInfo::getStatus,OrderStatusEnums.ORDER_CANCEL.getCode())
                        .eq(WmsSaleOrderInfo::getId,id)
        );
    }

    @Override
    public void restartOrderById(String id) {
        update(
                Wrappers.<WmsSaleOrderInfo>lambdaUpdate().set(WmsSaleOrderInfo::getStatus,OrderStatusEnums.ORDER_NEW.getCode())
                        .eq(WmsSaleOrderInfo::getId,id)
        );
    }

    @Override
    public WmsSaleOrderInfo getByOrderNo(String orderNo) {
        return getOne(Wrappers.<WmsSaleOrderInfo>lambdaQuery().eq(WmsSaleOrderInfo::getOrderNo,orderNo).last("limit 1"));
    }
}
