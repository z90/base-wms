package com.ruoyi.wms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.wms.entity.WmsSupplierInfo;
import com.ruoyi.wms.mapper.WmsSupplierInfoMapper;
import com.ruoyi.wms.service.IWmsSupplierInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 供应商 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsSupplierInfoServiceImpl extends ServiceImpl<WmsSupplierInfoMapper, WmsSupplierInfo> implements IWmsSupplierInfoService {

    @Override
    public WmsSupplierInfo findByCode(String supplierCode) {
        return getOne(Wrappers.<WmsSupplierInfo>lambdaQuery().eq(WmsSupplierInfo::getSupplierCode,supplierCode).last("limit 1"));
    }
}
