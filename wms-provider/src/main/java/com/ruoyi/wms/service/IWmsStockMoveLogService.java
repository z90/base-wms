package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsStockMoveLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 移库记录 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsStockMoveLogService extends IService<WmsStockMoveLog> {

}
