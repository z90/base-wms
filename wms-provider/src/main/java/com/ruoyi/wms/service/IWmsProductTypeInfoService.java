package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsProductTypeInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分类 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsProductTypeInfoService extends IService<WmsProductTypeInfo> {

    /**
     * 通过编码获得类型
     * @param code
     * @return 类型信息
     */
    WmsProductTypeInfo getByCode(String code);
}
