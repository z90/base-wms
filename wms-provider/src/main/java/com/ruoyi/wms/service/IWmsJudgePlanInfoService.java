package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsJudgePlanInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 盘点计划信息 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsJudgePlanInfoService extends IService<WmsJudgePlanInfo> {

}
