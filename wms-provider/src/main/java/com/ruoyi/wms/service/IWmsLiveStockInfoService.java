package com.ruoyi.wms.service;

import com.ruoyi.wms.dto.WmsLiveStockInfoDto;
import com.ruoyi.wms.entity.WmsLiveStockInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.Valid;

/**
 * <p>
 * 实时库存 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsLiveStockInfoService extends IService<WmsLiveStockInfo> {


    /**
     * 入库
     * @param dto 入库信息
     * @return 入库是否成功
     */
    public boolean inStock(@Valid WmsLiveStockInfoDto dto);

    /**
     * 出库
     * @param dto 出库信息
     * @return 出库是否成功
     */
    public boolean outStock(@Valid WmsLiveStockInfoDto dto);

}
