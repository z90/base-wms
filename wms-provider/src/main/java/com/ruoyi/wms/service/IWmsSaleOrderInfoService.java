package com.ruoyi.wms.service;

import com.ruoyi.wms.controller.form.WmsSaleOrderInfoForm;
import com.ruoyi.wms.entity.WmsSaleOrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 销售订单 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsSaleOrderInfoService extends IService<WmsSaleOrderInfo> {

    /**
     * 新增销售订单
     * @param form 销售订单详情
     */
    void insertOrderInfo(WmsSaleOrderInfoForm form);

    /**
     * 取消订单
     * @param id 订单id
     */
    void cancelOrderById(String id);

    /**
     * 重启订单
     * @param id 订单id
     */
    void restartOrderById(String id);

    /**
     * 通过订单号获得订单信息
     * @param orderNo 订单号
     * @return 订单信息
     */
    WmsSaleOrderInfo getByOrderNo(String orderNo);
}
