package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsReferenceBook;
import com.ruoyi.wms.mapper.WmsReferenceBookMapper;
import com.ruoyi.wms.service.IWmsReferenceBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 备查簿 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsReferenceBookServiceImpl extends ServiceImpl<WmsReferenceBookMapper, WmsReferenceBook> implements IWmsReferenceBookService {

}
