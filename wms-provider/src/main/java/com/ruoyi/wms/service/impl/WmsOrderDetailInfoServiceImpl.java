package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsOrderDetailInfo;
import com.ruoyi.wms.mapper.WmsOrderDetailInfoMapper;
import com.ruoyi.wms.service.IWmsOrderDetailInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单详情 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsOrderDetailInfoServiceImpl extends ServiceImpl<WmsOrderDetailInfoMapper, WmsOrderDetailInfo> implements IWmsOrderDetailInfoService {

}
