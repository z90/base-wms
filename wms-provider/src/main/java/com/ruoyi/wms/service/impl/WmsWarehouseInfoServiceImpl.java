package com.ruoyi.wms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.exception.WmsBusinessException;
import com.ruoyi.wms.entity.WmsWarehouseInfo;
import com.ruoyi.wms.mapper.WmsWarehouseInfoMapper;
import com.ruoyi.wms.service.IWmsWarehouseInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * <p>
 * 仓库管理 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@Service
public class WmsWarehouseInfoServiceImpl extends ServiceImpl<WmsWarehouseInfoMapper, WmsWarehouseInfo> implements IWmsWarehouseInfoService {

    @Override
    public WmsWarehouseInfo getInfoByCode(String code) {
        return getOne(Wrappers.<WmsWarehouseInfo>lambdaQuery().eq(WmsWarehouseInfo::getWarehouseCode,code));
    }

    @Override
    public WmsWarehouseInfo getInfoById(String id) {
        return Optional.of(getById(id)).orElseThrow(()->new WmsBusinessException("仓库信息不存在"));

    }
}
