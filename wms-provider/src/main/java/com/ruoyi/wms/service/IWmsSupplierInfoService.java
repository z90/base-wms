package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsSupplierInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 供应商 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsSupplierInfoService extends IService<WmsSupplierInfo> {

    /**
     * 通过供应商编码获得供应商信息
     * @param supplierCode 供应商编码
     * @return 供应商信息
     */
    WmsSupplierInfo findByCode(String supplierCode);
}
