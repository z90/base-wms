package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsProductUnitConvert;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.wms.mapper.WmsProductUnitConvertMapper;
import com.ruoyi.wms.service.IWmsProductUnitConvertService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Slf4j
@Service
public class WmsProductUnitConvertServiceImpl extends ServiceImpl<WmsProductUnitConvertMapper, WmsProductUnitConvert> implements IWmsProductUnitConvertService {
}
