package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsJudgeDetailInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 判断详情 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsJudgeDetailInfoService extends IService<WmsJudgeDetailInfo> {

}
