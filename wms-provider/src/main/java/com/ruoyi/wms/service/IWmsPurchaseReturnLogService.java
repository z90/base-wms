package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsPurchaseReturnLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 销售退货 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsPurchaseReturnLogService extends IService<WmsPurchaseReturnLog> {

}
