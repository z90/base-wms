package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsContactInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 供应商联系人 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsContactInfoService extends IService<WmsContactInfo> {

}
