package com.ruoyi.wms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.wms.entity.WmsOutLog;
import com.ruoyi.wms.entity.WmsReferenceBook;
import com.ruoyi.wms.mapper.WmsLiveStockInfoMapper;
import com.ruoyi.wms.mapper.WmsOutLogMapper;
import com.ruoyi.wms.mapstruct.WmsReferenceBookMapStructMapper;
import com.ruoyi.wms.service.IWmsOutLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.wms.service.IWmsReferenceBookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 出库记录 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
@AllArgsConstructor
public class WmsOutLogServiceImpl extends ServiceImpl<WmsOutLogMapper, WmsOutLog> implements IWmsOutLogService {

    private final IWmsReferenceBookService iWmsReferenceBookService;
    private final WmsLiveStockInfoMapper wmsLiveStockInfoMapper;
    private final WmsReferenceBookMapStructMapper wmsReferenceBookMapStructMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveInfo(WmsOutLog outLog) {
        save(outLog);
        BigDecimal num = wmsLiveStockInfoMapper.getLiveCount(outLog.getProductCode());
        WmsReferenceBook book = wmsReferenceBookMapStructMapper.outLogToWmsReferenceBook(outLog);
        book.setBeforeNum(num);
        book.setUpdateNum(outLog.getNum());
        book.setAfterNum(num.subtract(outLog.getNum()));
        // 记录备查薄
        iWmsReferenceBookService.save(book);
    }
}
