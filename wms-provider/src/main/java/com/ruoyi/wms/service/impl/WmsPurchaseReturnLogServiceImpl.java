package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsPurchaseReturnLog;
import com.ruoyi.wms.mapper.WmsPurchaseReturnLogMapper;
import com.ruoyi.wms.service.IWmsPurchaseReturnLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 销售退货 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsPurchaseReturnLogServiceImpl extends ServiceImpl<WmsPurchaseReturnLogMapper, WmsPurchaseReturnLog> implements IWmsPurchaseReturnLogService {

}
