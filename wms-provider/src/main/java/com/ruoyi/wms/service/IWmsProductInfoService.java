package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsProductInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsProductInfoService extends IService<WmsProductInfo> {

    /**
     * 导入产品数据
     * @param wmsProductInfos
     * @return 导入结果
     */
    String importProduct(List<WmsProductInfo> wmsProductInfos, boolean updateSupport);

    /**
     * 通过产品编码获得产品信息
     * @param productCode 产品编码
     * @return 产品信息
     */
    WmsProductInfo getByCode(String productCode);
}
