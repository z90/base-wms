package com.ruoyi.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.wms.entity.WmsProductUnitConvert;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
public interface IWmsProductUnitConvertService extends IService<WmsProductUnitConvert> {
}
