package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsOrderDetailInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单详情 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsOrderDetailInfoService extends IService<WmsOrderDetailInfo> {

}
