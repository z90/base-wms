package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsJudgeDetailInfo;
import com.ruoyi.wms.mapper.WmsJudgeDetailInfoMapper;
import com.ruoyi.wms.service.IWmsJudgeDetailInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 判断详情 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsJudgeDetailInfoServiceImpl extends ServiceImpl<WmsJudgeDetailInfoMapper, WmsJudgeDetailInfo> implements IWmsJudgeDetailInfoService {

}
