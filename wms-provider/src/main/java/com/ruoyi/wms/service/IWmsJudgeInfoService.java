package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsJudgeInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 盘点 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsJudgeInfoService extends IService<WmsJudgeInfo> {

}
