package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsSaleReturnLog;
import com.ruoyi.wms.mapper.WmsSaleReturnLogMapper;
import com.ruoyi.wms.service.IWmsSaleReturnLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 销售退货 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsSaleReturnLogServiceImpl extends ServiceImpl<WmsSaleReturnLogMapper, WmsSaleReturnLog> implements IWmsSaleReturnLogService {

}
