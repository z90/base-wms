package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsStockMoveLog;
import com.ruoyi.wms.mapper.WmsStockMoveLogMapper;
import com.ruoyi.wms.service.IWmsStockMoveLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 移库记录 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsStockMoveLogServiceImpl extends ServiceImpl<WmsStockMoveLogMapper, WmsStockMoveLog> implements IWmsStockMoveLogService {

}
