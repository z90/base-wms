package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsWarehouseLocationInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 库位信息 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsWarehouseLocationInfoService extends IService<WmsWarehouseLocationInfo> {

    /**
     * 通过编码获得库位信息
     * @param locationCode 库位编码
     * @return 库位详情
     */
    WmsWarehouseLocationInfo getByCode(String locationCode);
}
