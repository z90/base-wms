package com.ruoyi.wms.service;

import com.ruoyi.wms.controller.form.WmsPurchaseOrderInfoForm;
import com.ruoyi.wms.entity.WmsPurchaseOrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 采购订单 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsPurchaseOrderInfoService extends IService<WmsPurchaseOrderInfo> {

    /**
     * 新增订单信息
     * @param form 订单信息
     */
    void insertOrderInfo(WmsPurchaseOrderInfoForm form);

    /**
     * 订单取消
     * @param id 订单id
     */
    void cancelOrderById(String id);

    /**
     * 重启订单
     * @param id 订单id
     */
    void restartOrderById(String id);

    /**
     * 通过订单号获得订单信息
     * @param orderNo 订单号
     * @return 订单信息
     */
    WmsPurchaseOrderInfo getByOrderNo(String orderNo);
}
