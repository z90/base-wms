package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsJudgeInfo;
import com.ruoyi.wms.mapper.WmsJudgeInfoMapper;
import com.ruoyi.wms.service.IWmsJudgeInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 盘点 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsJudgeInfoServiceImpl extends ServiceImpl<WmsJudgeInfoMapper, WmsJudgeInfo> implements IWmsJudgeInfoService {

}
