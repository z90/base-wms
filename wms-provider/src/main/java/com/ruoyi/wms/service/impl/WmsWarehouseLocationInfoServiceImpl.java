package com.ruoyi.wms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.wms.entity.WmsWarehouseInfo;
import com.ruoyi.wms.entity.WmsWarehouseLocationInfo;
import com.ruoyi.wms.mapper.WmsWarehouseLocationInfoMapper;
import com.ruoyi.wms.service.IWmsWarehouseLocationInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库位信息 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Slf4j
@Service
@AllArgsConstructor
public class WmsWarehouseLocationInfoServiceImpl extends ServiceImpl<WmsWarehouseLocationInfoMapper, WmsWarehouseLocationInfo> implements IWmsWarehouseLocationInfoService {

    @Override
    public WmsWarehouseLocationInfo getByCode(String locationCode) {
        return getOne(Wrappers.<WmsWarehouseLocationInfo>lambdaQuery().eq(WmsWarehouseLocationInfo::getLocationCode,locationCode));
    }
}
