package com.ruoyi.wms.service.impl;

import com.ruoyi.wms.entity.WmsFileInfo;
import com.ruoyi.wms.mapper.WmsFileInfoMapper;
import com.ruoyi.wms.service.IWmsFileInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsFileInfoServiceImpl extends ServiceImpl<WmsFileInfoMapper, WmsFileInfo> implements IWmsFileInfoService {

}
