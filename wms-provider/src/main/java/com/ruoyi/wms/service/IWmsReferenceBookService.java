package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsReferenceBook;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 备查簿 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsReferenceBookService extends IService<WmsReferenceBook> {


}
