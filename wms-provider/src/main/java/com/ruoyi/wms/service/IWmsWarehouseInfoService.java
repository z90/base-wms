package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsWarehouseInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 仓库管理 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsWarehouseInfoService extends IService<WmsWarehouseInfo> {


    /**
     * 通过仓库编码获得 仓库信息
     * @param code 仓库编码
     * @return 仓库信息
     */
    WmsWarehouseInfo getInfoByCode(String code);

    /**
     * 通过id获得仓库信息
     * @param id 仓库id
     * @return 仓库信息
     */
    WmsWarehouseInfo getInfoById(String id);
}
