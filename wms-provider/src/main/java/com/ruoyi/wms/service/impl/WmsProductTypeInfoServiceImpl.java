package com.ruoyi.wms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.wms.entity.WmsProductInfo;
import com.ruoyi.wms.entity.WmsProductTypeInfo;
import com.ruoyi.wms.mapper.WmsProductTypeInfoMapper;
import com.ruoyi.wms.service.IWmsProductTypeInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类 服务实现类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Service
public class WmsProductTypeInfoServiceImpl extends ServiceImpl<WmsProductTypeInfoMapper, WmsProductTypeInfo> implements IWmsProductTypeInfoService {

    @Override
    public WmsProductTypeInfo getByCode(String code) {
        return getOne(Wrappers.<WmsProductTypeInfo>lambdaQuery().eq(WmsProductTypeInfo::getTypeCode,code).last("limit 1"));
    }
}
