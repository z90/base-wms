package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsInLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 入库记录 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsInLogService extends IService<WmsInLog> {

    /**
     * 保存入库日志
     * @param wmsInLog 入库信息
     */
    void saveInfo(WmsInLog wmsInLog);
}
