package com.ruoyi.wms.service;

import com.ruoyi.wms.entity.WmsCustomerInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户信息 服务类
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface IWmsCustomerInfoService extends IService<WmsCustomerInfo> {

    /**
     * 通过客户编号获得客户信息
     * @param customerCode 客户编号
     * @return 客户信息
     */
    WmsCustomerInfo findByCode(String customerCode);
}
