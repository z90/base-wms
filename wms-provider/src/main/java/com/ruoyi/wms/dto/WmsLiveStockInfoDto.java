package com.ruoyi.wms.dto;


import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 实时库存
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Data
public class WmsLiveStockInfoDto implements Serializable {


    /**
     * id
     */
    private String id;

    /**
     * 产品编码
     */
    @NotEmpty(message = "商品编号不能为空")
    private String productCode;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 单位 字典 wms_product_unit
     */
    private String productUnit;

    /**
     * 产品类型
     */
    private String productType;

    /**
     * 容器编码
     */
    private String containerCode;

    /**
     * 单价
     */
    private BigDecimal price;
    /**
     * 数量
     */
    @Min(value = 1,message = "数量不能少于{value}")
    private BigDecimal num;

    /**
     * 仓库编码
     */
    @NotEmpty(message = "仓库不存在")
    private String warehouseCode;

    /**
     * 库位编码
     */
    @NotEmpty(message = "库位不存在")
    private String locationCode;

    /**
     * 供应商编码
     */
    private String supplierCode;

    /**
     * 是否锁定：N:否 Y:是
     */
    private String lockFlag;

    /**
     * 乐观锁 
     */
    @Version
    private Integer version;

    /**
     * 类型 出库 入库 退货 报废等等
     */
    @NotEmpty(message = "操作类型不能为空")
    private String actionType;


}
