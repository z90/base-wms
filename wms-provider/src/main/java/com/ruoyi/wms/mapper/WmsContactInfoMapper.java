package com.ruoyi.wms.mapper;

import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsContactInfo;

/**
 * <p>
 * 供应商联系人 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsContactInfoMapper extends DataTenantMapper<WmsContactInfo> {

}
