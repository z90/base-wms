package com.ruoyi.wms.mapper;

import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsSupplierInfo;

/**
 * <p>
 * 供应商 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsSupplierInfoMapper extends DataTenantMapper<WmsSupplierInfo> {

}
