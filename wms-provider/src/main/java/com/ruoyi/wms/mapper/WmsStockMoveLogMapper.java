package com.ruoyi.wms.mapper;

import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsStockMoveLog;

/**
 * <p>
 * 移库记录 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsStockMoveLogMapper extends DataTenantMapper<WmsStockMoveLog> {

}
