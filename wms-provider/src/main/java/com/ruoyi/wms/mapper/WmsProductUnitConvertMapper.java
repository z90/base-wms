package com.ruoyi.wms.mapper;

import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsProductUnitConvert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Mapper
public interface WmsProductUnitConvertMapper extends DataTenantMapper<WmsProductUnitConvert> {
}
