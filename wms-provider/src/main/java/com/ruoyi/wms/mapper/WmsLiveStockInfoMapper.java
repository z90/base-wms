package com.ruoyi.wms.mapper;

import com.ruoyi.common.annotation.DataTenantDataScope;
import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsLiveStockInfo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 实时库存 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsLiveStockInfoMapper extends DataTenantMapper<WmsLiveStockInfo> {

    @DataTenantDataScope
    BigDecimal getLiveCount(@Param("productCode") String productCode);
}
