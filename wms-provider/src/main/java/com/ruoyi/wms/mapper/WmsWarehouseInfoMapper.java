package com.ruoyi.wms.mapper;

import com.ruoyi.common.annotation.DataTenantDataScope;
import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsWarehouseInfo;

/**
 * <p>
 * 仓库管理 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsWarehouseInfoMapper extends DataTenantMapper<WmsWarehouseInfo> {

    @DataTenantDataScope
    public WmsWarehouseInfo getInfoById(String id);
}
