package com.ruoyi.wms.mapper;

import com.ruoyi.common.core.mapper.DataTenantMapper;
import com.ruoyi.wms.entity.WmsWarehouseLocationInfo;

/**
 * <p>
 * 库位信息 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsWarehouseLocationInfoMapper extends DataTenantMapper<WmsWarehouseLocationInfo> {

}
