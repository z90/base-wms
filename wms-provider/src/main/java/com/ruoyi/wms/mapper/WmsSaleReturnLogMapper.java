package com.ruoyi.wms.mapper;

import com.ruoyi.wms.entity.WmsSaleReturnLog;
import com.ruoyi.common.core.mapper.DataTenantMapper;

/**
 * <p>
 * 销售退货 Mapper 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
public interface WmsSaleReturnLogMapper extends DataTenantMapper<WmsSaleReturnLog> {

}
