package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsProductUnitConvertForm;
import com.ruoyi.wms.controller.vo.WmsProductUnitConvertVo;
import com.ruoyi.wms.entity.WmsProductUnitConvert;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author zxg
 * @date 2023-11-08
 * @Desc
 */
@Mapper(componentModel = "spring")
public interface WmsProductUnitConvertStructMapper {

    WmsProductUnitConvertVo infoToVo(WmsProductUnitConvert wmsProductUnitConvert);
    List<WmsProductUnitConvertVo> infosToVos(List<WmsProductUnitConvert> wmsProductUnitConverts);
    WmsProductUnitConvert fromToInfo(WmsProductUnitConvertForm form);

}
