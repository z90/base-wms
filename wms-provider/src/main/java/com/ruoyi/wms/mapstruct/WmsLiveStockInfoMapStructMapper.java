package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsLiveStockInfoForm;
import com.ruoyi.wms.controller.vo.WmsLiveStockInfoVo;
import com.ruoyi.wms.dto.WmsLiveStockInfoDto;
import com.ruoyi.wms.entity.WmsLiveStockInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 实时库存 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsLiveStockInfoMapStructMapper {
    public WmsLiveStockInfo formToWmsLiveStockInfo(WmsLiveStockInfoForm form);
    public WmsLiveStockInfoVo wmsLiveStockInfoToVo(WmsLiveStockInfo wmsLiveStockInfo);
    public List<WmsLiveStockInfoVo> wmsLiveStockInfoToVo(List<WmsLiveStockInfo> wmsLiveStockInfos);

    public WmsLiveStockInfo dtoToWmsLiveStockInfo(WmsLiveStockInfoDto dto);
}
