package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsSaleReturnLogForm;
import com.ruoyi.wms.controller.vo.WmsSaleReturnLogVo;
import com.ruoyi.wms.entity.WmsSaleReturnLog;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 销售退货 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsSaleReturnLogMapStructMapper {
    public WmsSaleReturnLog formToWmsSaleReturnLog(WmsSaleReturnLogForm form);
    public WmsSaleReturnLogVo wmsSaleReturnLogToVo(WmsSaleReturnLog wmsSaleReturnLog);
    public List<WmsSaleReturnLogVo> wmsSaleReturnLogToVo(List<WmsSaleReturnLog> wmsSaleReturnLogs);
}
