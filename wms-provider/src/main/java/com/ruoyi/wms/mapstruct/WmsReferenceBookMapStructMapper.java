package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsReferenceBookForm;
import com.ruoyi.wms.controller.vo.WmsReferenceBookVo;
import com.ruoyi.wms.entity.WmsInLog;
import com.ruoyi.wms.entity.WmsOutLog;
import com.ruoyi.wms.entity.WmsReferenceBook;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 备查簿 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsReferenceBookMapStructMapper {
    public WmsReferenceBook formToWmsReferenceBook(WmsReferenceBookForm form);
    public WmsReferenceBookVo wmsReferenceBookToVo(WmsReferenceBook wmsReferenceBook);
    public List<WmsReferenceBookVo> wmsReferenceBookToVo(List<WmsReferenceBook> wmsReferenceBooks);

    public WmsReferenceBook inLogToWmsReferenceBook(WmsInLog inLog);
    public WmsReferenceBook outLogToWmsReferenceBook(WmsOutLog outLog);


}
