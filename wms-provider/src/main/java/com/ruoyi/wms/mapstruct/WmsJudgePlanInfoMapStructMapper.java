package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsJudgePlanInfoForm;
import com.ruoyi.wms.controller.vo.WmsJudgePlanInfoVo;
import com.ruoyi.wms.entity.WmsJudgePlanInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 盘点计划信息 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsJudgePlanInfoMapStructMapper {
    public WmsJudgePlanInfo formToWmsJudgePlanInfo(WmsJudgePlanInfoForm form);
    public WmsJudgePlanInfoVo wmsJudgePlanInfoToVo(WmsJudgePlanInfo wmsJudgePlanInfo);
    public List<WmsJudgePlanInfoVo> wmsJudgePlanInfoToVo(List<WmsJudgePlanInfo> wmsJudgePlanInfos);
}
