package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsJudgeInfoForm;
import com.ruoyi.wms.controller.vo.WmsJudgeInfoVo;
import com.ruoyi.wms.entity.WmsJudgeInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 盘点 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsJudgeInfoMapStructMapper {
    public WmsJudgeInfo formToWmsJudgeInfo(WmsJudgeInfoForm form);
    public WmsJudgeInfoVo wmsJudgeInfoToVo(WmsJudgeInfo wmsJudgeInfo);
    public List<WmsJudgeInfoVo> wmsJudgeInfoToVo(List<WmsJudgeInfo> wmsJudgeInfos);
}
