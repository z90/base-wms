package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.excel.WmsCustomerInfoExcel;
import com.ruoyi.wms.controller.form.WmsCustomerInfoForm;
import com.ruoyi.wms.controller.vo.WmsCustomerInfoVo;
import com.ruoyi.wms.entity.WmsCustomerInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 客户信息 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsCustomerInfoMapStructMapper {
    public WmsCustomerInfo formToWmsCustomerInfo(WmsCustomerInfoForm form);
    public WmsCustomerInfoVo wmsCustomerInfoToVo(WmsCustomerInfo wmsCustomerInfo);
    public List<WmsCustomerInfoVo> wmsCustomerInfoToVo(List<WmsCustomerInfo> wmsCustomerInfos);

    public WmsCustomerInfoExcel infoToExcel(WmsCustomerInfo wmsCustomerInfo);

    public List<WmsCustomerInfoExcel> infosToExcels(List<WmsCustomerInfo> wmsCustomerInfos);
}
