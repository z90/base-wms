package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsWarehouseInfoForm;
import com.ruoyi.wms.controller.vo.WmsWarehouseInfoVo;
import com.ruoyi.wms.entity.WmsWarehouseInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 仓库管理 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsWarehouseInfoMapStructMapper {
    public WmsWarehouseInfo formToWmsWarehouseInfo(WmsWarehouseInfoForm form);
    public WmsWarehouseInfoVo wmsWarehouseInfoToVo(WmsWarehouseInfo wmsWarehouseInfo);
    public List<WmsWarehouseInfoVo> wmsWarehouseInfoToVo(List<WmsWarehouseInfo> wmsWarehouseInfos);
}
