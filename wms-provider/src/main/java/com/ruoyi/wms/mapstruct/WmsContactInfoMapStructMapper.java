package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.excel.WmsContactInfoExcel;
import com.ruoyi.wms.controller.form.WmsContactInfoForm;
import com.ruoyi.wms.controller.vo.WmsContactInfoVo;
import com.ruoyi.wms.entity.WmsContactInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 供应商联系人 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsContactInfoMapStructMapper {
    public WmsContactInfo formToWmsContactInfo(WmsContactInfoForm form);
    public WmsContactInfoVo wmsContactInfoToVo(WmsContactInfo wmsContactInfo);
    public List<WmsContactInfoVo> wmsContactInfoToVo(List<WmsContactInfo> wmsContactInfos);

    public WmsContactInfoExcel infoToExcel(WmsContactInfo wmsContactInfo);

    public List<WmsContactInfoExcel> infosToExcels(List<WmsContactInfo> wmsContactInfos);
}
