package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsStockMoveLogForm;
import com.ruoyi.wms.controller.vo.WmsStockMoveLogVo;
import com.ruoyi.wms.entity.WmsStockMoveLog;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 移库记录 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsStockMoveLogMapStructMapper {
    public WmsStockMoveLog formToWmsStockMoveLog(WmsStockMoveLogForm form);
    public WmsStockMoveLogVo wmsStockMoveLogToVo(WmsStockMoveLog wmsStockMoveLog);
    public List<WmsStockMoveLogVo> wmsStockMoveLogToVo(List<WmsStockMoveLog> wmsStockMoveLogs);
}
