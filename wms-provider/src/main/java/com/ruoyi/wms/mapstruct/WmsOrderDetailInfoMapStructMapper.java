package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.excel.WmsOrderDetailInfoExcel;
import com.ruoyi.wms.controller.form.WmsOrderDetailInfoForm;
import com.ruoyi.wms.controller.vo.WmsOrderDetailInfoVo;
import com.ruoyi.wms.entity.WmsOrderDetailInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 订单详情 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsOrderDetailInfoMapStructMapper {
    public WmsOrderDetailInfo formToWmsOrderDetailInfo(WmsOrderDetailInfoForm form);
    public WmsOrderDetailInfoVo wmsOrderDetailInfoToVo(WmsOrderDetailInfo wmsOrderDetailInfo);
    public List<WmsOrderDetailInfoVo> wmsOrderDetailInfoToVo(List<WmsOrderDetailInfo> wmsOrderDetailInfos);

    public WmsOrderDetailInfoExcel infoToExcel(WmsOrderDetailInfo wmsOrderDetailInfo);

    public List<WmsOrderDetailInfoExcel> infosToExcels(List<WmsOrderDetailInfo> wmsOrderDetailInfos);
}
