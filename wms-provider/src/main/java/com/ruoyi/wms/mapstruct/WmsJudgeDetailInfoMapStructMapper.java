package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsJudgeDetailInfoForm;
import com.ruoyi.wms.controller.vo.WmsJudgeDetailInfoVo;
import com.ruoyi.wms.entity.WmsJudgeDetailInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 判断详情 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsJudgeDetailInfoMapStructMapper {
    public WmsJudgeDetailInfo formToWmsJudgeDetailInfo(WmsJudgeDetailInfoForm form);
    public WmsJudgeDetailInfoVo wmsJudgeDetailInfoToVo(WmsJudgeDetailInfo wmsJudgeDetailInfo);
    public List<WmsJudgeDetailInfoVo> wmsJudgeDetailInfoToVo(List<WmsJudgeDetailInfo> wmsJudgeDetailInfos);
}
