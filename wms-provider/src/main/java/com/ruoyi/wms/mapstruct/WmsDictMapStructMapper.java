package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.vo.WmsDictVo;
import com.ruoyi.wms.entity.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

/**
 * @author zxg
 * @date 2023-08-18
 * @Desc
 */
@Mapper(componentModel = "spring")
public interface WmsDictMapStructMapper {
    @Mappings({
            @Mapping(source = "typeName",target = "label"),
            @Mapping(source = "id",target = "value")
    })
    WmsDictVo productTypeToDict(WmsProductTypeInfo wmsProductTypeInfo);

    List<WmsDictVo> productTypesToDicts(List<WmsProductTypeInfo> rainRooms);

    @Mappings({
            @Mapping(source = "warehouseName",target = "label"),
            @Mapping(source = "warehouseCode",target = "code"),
            @Mapping(source = "id",target = "value")
    })
    WmsDictVo warehouseToDict(WmsWarehouseInfo wmsWarehouseInfo);

    List<WmsDictVo> warehousesToDicts(List<WmsWarehouseInfo> wmsWarehouseInfos);

    @Mappings({
            @Mapping(source = "productName",target = "label"),
            @Mapping(source = "productCode",target = "value")
    })
    WmsDictVo productToDict(WmsProductInfo wmsProductInfo);

    List<WmsDictVo> productsToDicts(List<WmsProductInfo> wmsProductInfos);

    @Mappings({
            @Mapping(source = "locationName",target = "label"),
            @Mapping(source = "locationCode",target = "value")
    })
    WmsDictVo locationCodeToDict(WmsWarehouseLocationInfo wmsWarehouseLocationInfo);

    List<WmsDictVo> locationCodesToDicts(List<WmsWarehouseLocationInfo> wmsWarehouseLocationInfos);

    @Mappings({
            @Mapping(source = "supplierName",target = "label"),
            @Mapping(source = "supplierCode",target = "value"),
            @Mapping(source = "id",target = "code")
    })
    WmsDictVo supplierToDict(WmsSupplierInfo wmsSupplierInfo);

    List<WmsDictVo> suppliersToDicts(List<WmsSupplierInfo> wmsSupplierInfos);

    @Mappings({
            @Mapping(source = "customerName",target = "label"),
            @Mapping(source = "customerCode",target = "value"),
            @Mapping(source = "id",target = "code")
    })
    WmsDictVo customerToDict(WmsCustomerInfo wmsCustomerInfo);

    List<WmsDictVo> customersToDicts(List<WmsCustomerInfo> wmsCustomerInfos);
    @Mappings({
            @Mapping(source = "userName",target = "label"),
            @Mapping(source = "userTel",target = "code"),
            @Mapping(source = "id",target = "value")
    })
    WmsDictVo contactToDict(WmsContactInfo wmsContactInfo);

    List<WmsDictVo> contactsToDicts(List<WmsContactInfo> wmsContactInfos);

}
