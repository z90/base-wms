package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.excel.WmsSupplierInfoExcel;
import com.ruoyi.wms.controller.form.WmsSupplierInfoForm;
import com.ruoyi.wms.controller.vo.WmsSupplierInfoVo;
import com.ruoyi.wms.entity.WmsSupplierInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 供应商 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsSupplierInfoMapStructMapper {
    public WmsSupplierInfo formToWmsSupplierInfo(WmsSupplierInfoForm form);
    public WmsSupplierInfoVo wmsSupplierInfoToVo(WmsSupplierInfo wmsSupplierInfo);
    public List<WmsSupplierInfoVo> wmsSupplierInfoToVo(List<WmsSupplierInfo> wmsSupplierInfos);

    public WmsSupplierInfoExcel infoToExcel(WmsSupplierInfo supplierInfo);

    public List<WmsSupplierInfoExcel> infosToExcels(List<WmsSupplierInfo> wmsSupplierInfos);
}
