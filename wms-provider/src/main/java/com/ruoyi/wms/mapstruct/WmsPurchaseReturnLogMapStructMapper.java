package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsPurchaseReturnLogForm;
import com.ruoyi.wms.controller.vo.WmsPurchaseReturnLogVo;
import com.ruoyi.wms.entity.WmsPurchaseReturnLog;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 销售退货 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsPurchaseReturnLogMapStructMapper {
    public WmsPurchaseReturnLog formToWmsPurchaseReturnLog(WmsPurchaseReturnLogForm form);
    public WmsPurchaseReturnLogVo wmsPurchaseReturnLogToVo(WmsPurchaseReturnLog wmsPurchaseReturnLog);
    public List<WmsPurchaseReturnLogVo> wmsPurchaseReturnLogToVo(List<WmsPurchaseReturnLog> wmsPurchaseReturnLogs);
}
