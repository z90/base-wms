package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsFileInfoForm;
import com.ruoyi.wms.controller.vo.WmsFileInfoVo;
import com.ruoyi.wms.entity.WmsFileInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 *  mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsFileInfoMapStructMapper {
    public WmsFileInfo formToWmsFileInfo(WmsFileInfoForm form);
    public WmsFileInfoVo wmsFileInfoToVo(WmsFileInfo wmsFileInfo);
    public List<WmsFileInfoVo> wmsFileInfoToVo(List<WmsFileInfo> wmsFileInfos);
}
