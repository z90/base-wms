package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsSaleOrderInfoForm;
import com.ruoyi.wms.controller.vo.WmsSaleOrderInfoVo;
import com.ruoyi.wms.entity.WmsSaleOrderInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 销售订单 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsSaleOrderInfoMapStructMapper {
    public WmsSaleOrderInfo formToWmsSaleOrderInfo(WmsSaleOrderInfoForm form);
    public WmsSaleOrderInfoVo wmsSaleOrderInfoToVo(WmsSaleOrderInfo wmsSaleOrderInfo);
    public List<WmsSaleOrderInfoVo> wmsSaleOrderInfoToVo(List<WmsSaleOrderInfo> wmsSaleOrderInfos);
}
