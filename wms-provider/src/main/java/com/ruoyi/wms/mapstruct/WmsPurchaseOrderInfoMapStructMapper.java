package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsPurchaseOrderInfoForm;
import com.ruoyi.wms.controller.vo.WmsPurchaseOrderInfoVo;
import com.ruoyi.wms.entity.WmsPurchaseOrderInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 采购订单 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsPurchaseOrderInfoMapStructMapper {
    public WmsPurchaseOrderInfo formToWmsPurchaseOrderInfo(WmsPurchaseOrderInfoForm form);
    public WmsPurchaseOrderInfoVo wmsPurchaseOrderInfoToVo(WmsPurchaseOrderInfo wmsPurchaseOrderInfo);
    public List<WmsPurchaseOrderInfoVo> wmsPurchaseOrderInfoToVo(List<WmsPurchaseOrderInfo> wmsPurchaseOrderInfos);
}
