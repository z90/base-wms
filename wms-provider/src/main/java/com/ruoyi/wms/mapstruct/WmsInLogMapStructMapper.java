package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsInLogForm;
import com.ruoyi.wms.controller.vo.WmsInLogVo;
import com.ruoyi.wms.dto.WmsLiveStockInfoDto;
import com.ruoyi.wms.entity.WmsInLog;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 入库记录 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsInLogMapStructMapper {
    public WmsInLog formToWmsInLog(WmsInLogForm form);
    public WmsInLogVo wmsInLogToVo(WmsInLog wmsInLog);
    public List<WmsInLogVo> wmsInLogToVo(List<WmsInLog> wmsInLogs);

    public WmsInLog stockDtoToWmsInLog(WmsLiveStockInfoDto dto);
}
