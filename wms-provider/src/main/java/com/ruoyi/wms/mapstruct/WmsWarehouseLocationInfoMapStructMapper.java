package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsWarehouseLocationInfoForm;
import com.ruoyi.wms.controller.vo.WmsWarehouseLocationInfoVo;
import com.ruoyi.wms.entity.WmsWarehouseLocationInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 库位信息 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsWarehouseLocationInfoMapStructMapper {
    public WmsWarehouseLocationInfo formToWmsWarehouseLocationInfo(WmsWarehouseLocationInfoForm form);
    public WmsWarehouseLocationInfoVo wmsWarehouseLocationInfoToVo(WmsWarehouseLocationInfo wmsWarehouseLocationInfo);
    public List<WmsWarehouseLocationInfoVo> wmsWarehouseLocationInfoToVo(List<WmsWarehouseLocationInfo> wmsWarehouseLocationInfos);
}
