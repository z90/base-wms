package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.excel.WmsProductInfoExcel;
import com.ruoyi.wms.controller.form.WmsProductInfoForm;
import com.ruoyi.wms.controller.vo.WmsProductInfoVo;
import com.ruoyi.wms.entity.WmsProductInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 商品表 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsProductInfoMapStructMapper {
    public WmsProductInfo formToWmsProductInfo(WmsProductInfoForm form);
    public WmsProductInfoVo wmsProductInfoToVo(WmsProductInfo wmsProductInfo);
    public List<WmsProductInfoVo> wmsProductInfoToVo(List<WmsProductInfo> wmsProductInfos);

    public WmsProductInfoExcel infoToExcel(WmsProductInfo wmsProductInfo);

    public List<WmsProductInfoExcel> infosToExcels(List<WmsProductInfo> wmsProductInfos);


    public WmsProductInfo excelToInfo(WmsProductInfoExcel excel);

    public List<WmsProductInfo> excelsToInfos(List<WmsProductInfoExcel> excels);
}
