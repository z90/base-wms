package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsOutLogForm;
import com.ruoyi.wms.controller.vo.WmsOutLogVo;
import com.ruoyi.wms.dto.WmsLiveStockInfoDto;
import com.ruoyi.wms.entity.WmsOutLog;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 出库记录 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsOutLogMapStructMapper {
    public WmsOutLog formToWmsOutLog(WmsOutLogForm form);
    public WmsOutLogVo wmsOutLogToVo(WmsOutLog wmsOutLog);
    public List<WmsOutLogVo> wmsOutLogToVo(List<WmsOutLog> wmsOutLogs);
    public WmsOutLog stockDtoToLog(WmsLiveStockInfoDto dto);
}
