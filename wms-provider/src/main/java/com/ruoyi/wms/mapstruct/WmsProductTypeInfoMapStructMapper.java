package com.ruoyi.wms.mapstruct;

import com.ruoyi.wms.controller.form.WmsProductTypeInfoForm;
import com.ruoyi.wms.controller.vo.WmsProductTypeInfoVo;
import com.ruoyi.wms.entity.WmsProductTypeInfo;
import org.mapstruct.Mapper;
import java.util.List;

/**
 * <p>
 * 商品分类 mapstruct 接口
 * </p>
 *
 * @author 张晓光
 * @since 2023-10-26
 */
@Mapper(componentModel = "spring")
public interface WmsProductTypeInfoMapStructMapper {
    public WmsProductTypeInfo formToWmsProductTypeInfo(WmsProductTypeInfoForm form);
    public WmsProductTypeInfoVo wmsProductTypeInfoToVo(WmsProductTypeInfo wmsProductTypeInfo);
    public List<WmsProductTypeInfoVo> wmsProductTypeInfoToVo(List<WmsProductTypeInfo> wmsProductTypeInfos);
}
