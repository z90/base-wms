import request from '@/utils/request'

const prefix = 'wmsCustomerInfo'

/**
 * 新增数据
 * @param params
 * @returns {*}
 */
export function addObj (params) {
  return request({
    url: `/${prefix}/saveWmsCustomerInfo`,
    method: 'POST',
    data: {
      ...params
    }
  })
}

/**
 * 删除数据
 * @param id
 * @returns {*}
 */
export function delObj (id) {
  return request({
    url: `/${prefix}/delWmsCustomerInfo/${id}`,
    method: 'DELETE'
  })
}

/**
 * 更新数据
 * @param params
 * @returns {*}
 */
export function editObj (params) {
  return request({
    url: `/${prefix}/updWmsCustomerInfo`,
    method: 'PUT',
    data: {
      ...params
    }
  })
}

/**
 * 通过id获得数据
 * @param id
 * @returns {*}
 */
export function findById (id) {
  return request({
    url: `/${prefix}/findWmsCustomerInfoById/${id}`,
    method: 'GET'
  })
}

/**
 * 获得分页数据
 * @param params
 * @returns {*}
 */
export function findAllList (params) {
  return request({
    url: `/${prefix}/findWmsCustomerInfoList`,
    method: 'GET',
    params: {
      ...params
    }
  })
}


/**
 * 获得字典数据
 * @returns {AxiosPromise}
 */
export function getDictList () {
  return request({
    url: `/${prefix}/getDictList`,
    method: 'GET'
  })
}
