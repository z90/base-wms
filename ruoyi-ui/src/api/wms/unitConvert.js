import request from '@/utils/request'

const prefix = 'wmsProductUnitConvert'

/**
 * 新增数据
 * @param params
 * @returns {*}
 */
export function addObj (params) {
  return request({
    url: `/${prefix}/saveWmsProductUnitConvert`,
    method: 'POST',
    data: {
      ...params
    }
  })
}

/**
 * 删除数据
 * @param id
 * @returns {*}
 */
export function delObj (id) {
  return request({
    url: `/${prefix}/delWmsProductUnitConvert/${id}`,
    method: 'DELETE'
  })
}

/**
 * 更新数据
 * @param params
 * @returns {*}
 */
export function editObj (params) {
  return request({
    url: `/${prefix}/updWmsProductUnitConvert`,
    method: 'PUT',
    data: {
      ...params
    }
  })
}

/**
 * 通过id获得数据
 * @param id
 * @returns {*}
 */
export function findById (id) {
  return request({
    url: `/${prefix}/findWmsProductUnitConvertById/${id}`,
    method: 'GET'
  })
}

/**
 * 获得分页数据
 * @param params
 * @returns {*}
 */
export function findAllList (params) {
  return request({
    url: `/${prefix}/findWmsProductUnitConvertList`,
    method: 'GET',
    params: {
      ...params
    }
  })
}
