import request from '@/utils/request'

const prefix = 'wmsPurchaseOrderInfo'

/**
 * 新增数据
 * @param params
 * @returns {*}
 */
export function addObj (params) {
  return request({
    url: `/${prefix}/saveWmsPurchaseOrderInfo`,
    method: 'POST',
    data: {
      ...params
    }
  })
}

/**
 * 删除数据
 * @param id
 * @returns {*}
 */
export function delObj (id) {
  return request({
    url: `/${prefix}/delWmsPurchaseOrderInfo/${id}`,
    method: 'DELETE'
  })
}

/**
 * 取消
 * @param id
 * @returns {*}
 */
export function cancelOrder (id) {
  return request({
    url: `/${prefix}/cancelOrder/${id}`,
    method: 'DELETE'
  })
}

/**
 * 重启
 * @param id
 * @returns {*}
 */
export function restartOrder (id) {
  return request({
    url: `/${prefix}/restartOrder/${id}`,
    method: 'DELETE'
  })
}


/**
 * 通过id获得数据
 * @param id
 * @returns {*}
 */
export function findById (id) {
  return request({
    url: `/${prefix}/findWmsPurchaseOrderInfoById/${id}`,
    method: 'GET'
  })
}

/**
 * 通过订单号获得订单详情
 * @param orderNo 订单号
 * @returns {*}
 */
export function findWmsPurchaseOrderInfoByOrderNo (orderNo) {
  return request({
    url: `/${prefix}/findWmsPurchaseOrderInfoByOrderNo/${orderNo}`,
    method: 'GET'
  })
}
/**
 * 获得分页数据
 * @param params
 * @returns {*}
 */
export function findAllList (params) {
  return request({
    url: `/${prefix}/findWmsPurchaseOrderInfoList`,
    method: 'GET',
    params: {
      ...params
    }
  })
}

