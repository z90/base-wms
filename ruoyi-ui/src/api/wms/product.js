import request from '@/utils/request'

const prefix = 'wmsProductInfo'

/**
 * 新增数据
 * @param params
 * @returns {*}
 */
export function addObj (params) {
  return request({
    url: `/${prefix}/saveWmsProductInfo`,
    method: 'POST',
    data: {
      ...params
    }
  })
}

/**
 * 删除数据
 * @param id
 * @returns {*}
 */
export function delObj (id) {
  return request({
    url: `/${prefix}/delWmsProductInfo/${id}`,
    method: 'DELETE'
  })
}

/**
 * 更新数据
 * @param params
 * @returns {*}
 */
export function editObj (params) {
  return request({
    url: `/${prefix}/updWmsProductInfo`,
    method: 'PUT',
    data: {
      ...params
    }
  })
}

/**
 * 通过id获得数据
 * @param id
 * @returns {*}
 */
export function findById (id) {
  return request({
    url: `/${prefix}/findWmsProductInfoById/${id}`,
    method: 'GET'
  })
}

/**
 * 获得分页数据
 * @param params
 * @returns {*}
 */
export function findAllList (params) {
  return request({
    url: `/${prefix}/findWmsProductInfoList`,
    method: 'GET',
    params: {
      ...params
    }
  })
}

/**
 * 获得字典数据格式数据
 * @returns {*}
 */
export function getDictListByType (type) {
  return request({
    url: `/${prefix}/getDictListByType/${type}`,
    method: 'GET'
  })
}

/**
 * 获得所有产品 字典数据格式
 * @returns {AxiosPromise}
 */
export function getDictList () {
  return request({
    url: `/${prefix}/getDictList`,
    method: 'GET'
  })
}

/**
 * 获得产品列表
 * @param params 查询条件
 * @returns {AxiosPromise}
 */
export function getProductList (params) {
  return request({
    url: `/${prefix}/getProductList`,
    method: 'GET',
    params: {
      ...params
    }
  })
}
