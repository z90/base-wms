import request from '@/utils/request'

const prefix = 'wmsWarehouseInfo'

/**
 * 新增数据
 * @param params
 * @returns {*}
 */
export function addObj (params) {
  return request({
    url: `/${prefix}/saveWmsWarehouseInfo`,
    method: 'POST',
    data: {
      ...params
    }
  })
}

/**
 * 删除数据
 * @param id
 * @returns {*}
 */
export function delObj (id) {
  return request({
    url: `/${prefix}/delWmsWarehouseInfo/${id}`,
    method: 'DELETE'
  })
}

/**
 * 更新数据
 * @param params
 * @returns {*}
 */
export function editObj (params) {
  return request({
    url: `/${prefix}/updWmsWarehouseInfo`,
    method: 'PUT',
    data: {
      ...params
    }
  })
}

/**
 * 通过id获得数据
 * @param id
 * @returns {*}
 */
export function findById (id) {
  return request({
    url: `/${prefix}/findWmsWarehouseInfoById/${id}`,
    method: 'GET'
  })
}

/**
 * 获得分页数据
 * @param params
 * @returns {*}
 */
export function findAllList (params) {
  return request({
    url: `/${prefix}/findWmsWarehouseInfoList`,
    method: 'GET',
    params: {
      ...params
    }
  })
}

/**
 * 获得字典数据格式数据
 * @returns {*}
 */
export function getDictInfo () {
  return request({
    url: `/${prefix}/getDictList`,
    method: 'GET'
  })
}
