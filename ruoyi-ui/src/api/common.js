import request from '@/utils/request'

// 获取验证码
export function uploadFile(data) {
  return request({
    url: '/common/upload',
    method: 'post',
    data: data
  })
}

/**
 * 创建二维码
 * @param data
 * @returns {*}
 */
export function getQrInfo(data){
  return request({
    url: '/qr/createQr?data=' + data,
    method: 'get'
  })
}

/**
 * 获得二维码刷新时间
 * @param data
 * @returns {*}
 */
export function getQrTime(data){
  return request({
    url: '/qr/getQrTime',
    method: 'get'
  })
}
