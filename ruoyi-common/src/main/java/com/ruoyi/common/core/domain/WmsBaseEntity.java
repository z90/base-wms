package com.ruoyi.common.core.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author zxg
 * @date 2023-07-03
 * @Desc 基础类
 */
@Getter
@Setter
public class WmsBaseEntity {


    /**
     * 排序
     */
    private Integer orderBy;

    /**
     * 备注
     */
    private String remark;

    /**
     * 公司id
     */
    @TableField(fill = FieldFill.INSERT)
    private String companyId;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel;
}
