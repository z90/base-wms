package com.ruoyi.common.core.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruoyi.common.annotation.DataTenantDataScope;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author zxg
 * @date 2023-11-06
 * @Desc
 */
public interface DataTenantMapper<T> extends BaseMapper<T> {

    /**
     * 通过数据id获得数据详情
     * @param id 数据id
     * @return 数据详情
     */
    @Override
    @DataTenantDataScope
    T selectById(Serializable id);

    /**
     * 获得一条数据
     * @param queryWrapper 查询条件
     * @return 数据详情
     */
    @Override
    @DataTenantDataScope
    T selectOne(@Param("ew") Wrapper<T> queryWrapper);
    /**
     * 删除
     * @param id 删除id
     * @return 删除是否成功
     */
    @Override
    @DataTenantDataScope
    int deleteById(Serializable id);

    /**
     * 获得列表数据
     * @param queryWrapper 查询条件
     * @return 列表数据
     */
    @Override
    @DataTenantDataScope
    List<T> selectList(@Param("ew") Wrapper<T> queryWrapper);
    /**
     * 根据 entity 条件，查询全部记录（并翻页）
     *
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    @Override
    @DataTenantDataScope
    <E extends IPage<T>> E selectPage(E page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper 条件，查询全部记录（并翻页）
     *
     * @param page         分页查询条件
     * @param queryWrapper 实体对象封装操作类
     */
    @Override
    @DataTenantDataScope
    <E extends IPage<Map<String, Object>>> E selectMapsPage(E page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

}
