package com.ruoyi.common.core.service.impl;

import cn.hutool.core.date.DateUtil;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.core.service.SequenceService;
import com.ruoyi.common.enums.SequenceEnums;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author zxg
 * @date 2023-11-09
 * @Desc
 */
@Service("cgOrderSequenceService")
@AllArgsConstructor
public class CgOrderSequenceServiceImpl implements SequenceService {
    private final RedisCache redisCache;
    @Override
    public String generateSerialNumber() {
        String nd = DateUtil.format(new Date(), "yyyyMMdd");
        return redisCache.generateDateSerialNumber(CacheConstants.SEQUENCE, SequenceEnums.ORDER_CG.getCode()+nd);
    }
}
