package com.ruoyi.common.core.search;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-08-02
 * @Desc
 */
@Data
public class HlxBaseSearch {
    private String keyword;

}
