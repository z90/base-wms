package com.ruoyi.common.core.domain;

/**
 * @author zxg
 * @date 2023-07-07
 * @Desc 查询基础类
 */
public class HlxCursorEntity {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
