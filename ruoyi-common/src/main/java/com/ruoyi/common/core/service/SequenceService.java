package com.ruoyi.common.core.service;

import com.ruoyi.common.utils.uuid.UUID;

/**
 * @author zxg
 * @date 2023-11-09
 * @Desc
 */
public interface SequenceService {
    default String generateSerialNumber(String code){
        return UUID.fastUUID().toString();
    };

    String generateSerialNumber();
}
