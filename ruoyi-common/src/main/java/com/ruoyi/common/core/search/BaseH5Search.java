package com.ruoyi.common.core.search;

/**
 * @author zxg
 * @date 2023-07-07
 * @Desc 基础查询条件
 */
public class BaseH5Search {
    /**
     * 查询分页游标
     */
    private String nextCursor;

    /**
     * 条数
     */
    private Long size = 10L;

    public String getNextCursor() {
        return nextCursor;
    }

    public void setNextCursor(String nextCursor) {
        this.nextCursor = nextCursor;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
