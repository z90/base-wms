package com.ruoyi.common.core.service.impl;

import cn.hutool.core.date.DateUtil;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.core.service.SequenceService;
import com.ruoyi.common.exception.BizBusinessException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author zxg
 * @date 2023-11-17
 * @Desc
 */
@Service("commonSequenceService")
@AllArgsConstructor
public class CommonSequenceServiceImpl implements SequenceService {
    private final RedisCache redisCache;
    @Override
    public String generateSerialNumber(String code) {
        String nd = DateUtil.format(new Date(), "yyyyMMdd");
        return redisCache.generateDateSerialNumber(CacheConstants.SEQUENCE, code+nd);
    }

    @Override
    public String generateSerialNumber() {
        throw new BizBusinessException("不支持的方法");
    }
}
