package com.ruoyi.common.core.page;

import com.ruoyi.common.core.domain.HlxCursorEntity;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

/**
 * @author zxg
 * @date 2023-07-07
 * @Desc 返回列表数据及下页游标
 */
public class PageH5Info {


    /** 列表数据 */
    private List<? extends HlxCursorEntity> lists;

    /**
     * 下页游标
     */
    private String nextCursor;

    public List<? extends HlxCursorEntity> getLists() {
        return lists;
    }

    public void setLists(List<? extends HlxCursorEntity> lists) {
        this.lists = lists;
    }

    public String getNextCursor() {
        return nextCursor;
    }

    public void setNextCursor(String nextCursor) {
        this.nextCursor = nextCursor;
    }
}
