package com.ruoyi.common.enums;

import lombok.Getter;

/**
 * @author zxg
 * @date 2023-10-26
 * @Desc
 */
@Getter
public enum DelFlag {
    DEL_N(0,"未删除"),DEL_Y(-1,"删除");
    private final Integer code;
    private final String msg;

    DelFlag(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
