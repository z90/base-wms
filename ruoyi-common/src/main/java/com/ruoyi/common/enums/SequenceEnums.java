package com.ruoyi.common.enums;

/**
 * @author zxg
 * @date 2023-11-09
 * @Desc 订单前缀
 */
public enum SequenceEnums {

    ORDER_CG("CG","采购订单"),
    ORDER_CK("CK","出库订单"),
    ORDER_YK("YK","移库订单"),
    ORDER_CGTH("CGTH","采购退货订单"),
    ORDER_RK("RK","入库订单"),
    ORDRE_XS("XS","销售订单");

    private String code;
    private String msg;

    SequenceEnums(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
