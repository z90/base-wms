package com.ruoyi.common.enums;

/**
 * @author zxg
 * @date 2023-10-26
 * @Desc
 */
public enum YesOrNo {
    /**
     * Y
     */
    Y,
    /**
     * N
     */
    N
}
