package com.ruoyi.common.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.ruoyi.common.interceptor.DataTenantInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zxg
 * @date 2023/8/20
 * @Desc
 */
@Configuration
public class MybatisPlusConfig {
    @Autowired
    private RuoYiConfig ruoYiConfig;
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        interceptor.addInnerInterceptor(new DataTenantInterceptor(ruoYiConfig.isTenantFlag()));
        return interceptor;
    }
}
