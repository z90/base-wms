package com.ruoyi.common.exception;

/**
 * @author zxg
 * @date 2023-07-03
 * @Desc
 */
public class BizBusinessException extends RuntimeException{

    public BizBusinessException(String message) {
        super(message);
    }

}
