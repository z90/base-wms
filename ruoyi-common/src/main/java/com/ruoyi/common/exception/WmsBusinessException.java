package com.ruoyi.common.exception;

/**
 * @author zxg
 * @date 2023-10-26
 * @Desc
 */
public class WmsBusinessException extends RuntimeException{

    public WmsBusinessException() {
    }

    public WmsBusinessException(String message) {
        super(message);
    }

    public WmsBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public WmsBusinessException(Throwable cause) {
        super(cause);
    }

    public WmsBusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
