package com.ruoyi.common.exception;

/**
 * @author zxg
 * @date 2023-07-03
 * @Desc
 */
public class HlxBusinessException extends RuntimeException{

    public HlxBusinessException(String message) {
        super(message);
    }

}
