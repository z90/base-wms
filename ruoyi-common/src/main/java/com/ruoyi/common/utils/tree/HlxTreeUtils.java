package com.ruoyi.common.utils.tree;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zxg
 * @date 2023-08-24
 * @Desc
 */
public class HlxTreeUtils {


    /**
     * list to tree 递归实现
     * @param list 列表数据
     * @param pid 上级id
     * @return 数据list数据
     */
    public static List<? extends TreeDataBasePo> dealTreeInfo(List<? extends TreeDataBasePo> list,String pid){

        return list.stream().filter(item->item.getParentId().equals(pid))
                .peek(item->{
                    item.setChilds(dealTreeInfo(list,item.getId()));
                }).collect(Collectors.toList());
    }

    /**
     * list to tree 递归实现
     * @param list 列表数据
     * @return 树形数据
     */
    public static List<? extends TreeDataBasePo> dealTreeInfo(List<? extends TreeDataBasePo> list){
        return dealTreeInfo(list,"0");
    }

    /**
     * list to tree map groupby 实现
     * @param list 列表数据
     * @param pid 上级id
     * @return 树形数据
     */
    public static List<? extends TreeDataBasePo> getTreeMap(List<? extends TreeDataBasePo> list,String pid){
        Map<String,List<TreeDataBasePo>> map = list.stream().collect(Collectors.groupingBy(TreeDataBasePo::getParentId));
        list.forEach(item->item.setChilds(map.get(item.getId())));
        return list.stream().filter(item->item.getParentId().equals(pid)).collect(Collectors.toList());
    }
}
