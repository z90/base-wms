package com.ruoyi.common.utils.tree;

import lombok.Data;

import java.util.List;

/**
 * @author zxg
 * @date 2023-08-24
 * @Desc
 */
@Data
public class TreeDataBasePo {
    private String id;

    /**
     * 上级id
     */
    private String parentId;

    private List<? extends TreeDataBasePo> childs;

}
