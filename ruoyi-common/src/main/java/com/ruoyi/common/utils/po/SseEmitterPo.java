package com.ruoyi.common.utils.po;

import lombok.Data;

/**
 * @author zxg
 * @date 2023-08-17
 * @Desc
 */
@Data
public class SseEmitterPo<T> {
    /**
     * 状态编码
     */
    private String code = "200";

    /**
     * 提示信息
     */
    private String msg = "success";
    /**
     * 消息内容
     */
    private T data;
}
