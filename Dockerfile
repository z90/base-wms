# 基础镜像，当前新镜像是基于哪个镜像
FROM registry.cn-qingdao.aliyuncs.com/zxg7712/tcxxkj-openjdk8:v1.0.0
# 镜像的制作人
MAINTAINER 张晓光/971059634@qq.com/18363671109
# 在容器中创建挂载点 可以使用多个["1","2"]
VOLUME ["/lcgh/uploadPath","/lcgh/logs"]
# 定义参数
ARG JAR_FILE
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo 'Asia/Shanghai' > /etc/timezone
WORKDIR /
ADD ./ruoyi-admin/target/ruoyi-admin.jar app.jar
# 利用 chmod 可以藉以控制文件如何被他人所调用。
RUN chmod +x app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-Duser.timezone=GMT+08","-jar","app.jar"]
# 声明暴露的端口
EXPOSE 80
